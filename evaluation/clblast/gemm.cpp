#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <atf.h>
#include <json.hpp>
#include <clblast.h>
#include <timer.hpp>

void tune(size_t platform_id, size_t device_id, cl::CommandQueue &queue, const std::string &device_type,
          size_t M, size_t N, size_t K, bool transpose_a, bool transpose_b,
          const std::vector<float> &a, const std::vector<float> &b, const std::vector<float> &c,
          float alpha, float beta) {
    std::unordered_map<std::string,size_t> parameters;
    auto status = clblast::TuneXgemmDirect<float>(&queue(), M, N, K, 1.0f, parameters);
    if (status != clblast::StatusCode::kSuccess) {
        std::cerr << "Tuning failed!" << std::endl;
        exit(EXIT_SUCCESS);
    }

    // store best found configuration if valid
    std::map<std::string, int> plain_config;
    for (const auto &tp : parameters) {
        plain_config[tp.first] = tp.second;
    }
    std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
    config_file_dir.append("/results/")
            .append(device_type).append("/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id)).append("/clblast/");
    std::string config_file_name = config_file_dir;
    config_file_name.append("gemm_");
    config_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    config_file_name.append("_config.json");
    int ret = system(std::string("mkdir -p ").append(config_file_dir).c_str());
    if (ret != 0) {
        std::cerr << "Failed to create results directory." << std::endl;
        exit(EXIT_FAILURE);
    }
    std::ofstream config_file(config_file_name, std::ios::out | std::ios::trunc);
    config_file << std::setw(4) << nlohmann::json(plain_config);
    config_file.close();
}

void bench(size_t platform_id, size_t device_id, const cl::Context &context, cl::CommandQueue &queue, const cl::Device &device, const std::string &device_type,
           size_t M, size_t N, size_t K, bool transpose_a, bool transpose_b,
           const std::vector<float> &a, const std::vector<float> &b, const std::vector<float> &c,
           float alpha, float beta) {
    int ld_a = K, ld_b = N, ld_c = N;
    auto transpose_a_arg = clblast::Transpose::kNo;
    auto transpose_b_arg = clblast::Transpose::kNo;
    if (transpose_a) {
        transpose_a_arg = clblast::Transpose::kYes;
        ld_a = M;
    } else if (transpose_b) {
        transpose_b_arg = clblast::Transpose::kYes;
        ld_b = K;
    }


    auto buf_a = cl::Buffer(context, CL_MEM_READ_ONLY,  M * K * sizeof(float));
    queue.enqueueWriteBuffer(buf_a, CL_TRUE, 0, M * K * sizeof(float), a.data());
    auto buf_b = cl::Buffer(context, CL_MEM_READ_ONLY,  K * N * sizeof(float));
    queue.enqueueWriteBuffer(buf_b, CL_TRUE, 0, K * N * sizeof(float), b.data());
    auto buf_c = cl::Buffer(context, CL_MEM_READ_WRITE, M * N * sizeof(float));
    queue.enqueueWriteBuffer(buf_c, CL_TRUE, 0, M * N * sizeof(float), c.data());

    // read configuration
    std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
    config_file_dir.append("/results/")
            .append(device_type).append("/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id)).append("/clblast/");
    std::string config_file_name = config_file_dir;
    config_file_name.append("gemm_");
    config_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    config_file_name.append("_config.json");
    std::ifstream config_file(config_file_name, std::ios::in);
    if (config_file.fail()) {
        std::cerr << "Unable to open configuration file. Has CLBlast not yet been tuned for this input size?" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto plain_config = nlohmann::json::parse(config_file);
    std::unordered_map<std::string, size_t> override_parameters;
    override_parameters["WGD"]    = plain_config.at("WGD").get<int>();
    override_parameters["MDIMCD"] = plain_config.at("MDIMCD").get<int>();
    override_parameters["NDIMCD"] = plain_config.at("NDIMCD").get<int>();
    override_parameters["MDIMAD"] = plain_config.at("MDIMAD").get<int>();
    override_parameters["NDIMBD"] = plain_config.at("NDIMBD").get<int>();
    override_parameters["KWID"]   = plain_config.at("KWID").get<int>();
    override_parameters["VWMD"]   = plain_config.at("VWMD").get<int>();
    override_parameters["VWND"]   = plain_config.at("VWND").get<int>();
    override_parameters["PADA"]   = plain_config.at("PADA").get<int>();
    override_parameters["PADB"]   = plain_config.at("PADB").get<int>();
    clblast::OverrideParameters(device(), "XgemmDirect", clblast::Precision::kSingle, override_parameters);

    start_profiling(10, 200);
    clblast::Gemm<float>(clblast::Layout::kRowMajor, transpose_a_arg, transpose_b_arg,
                         M, N, K,
                         alpha,
                         buf_a(), 0, ld_a,
                         buf_b(), 0, ld_b,
                         beta,
                         buf_c(), 0, ld_c, &queue());
    auto profiling_data = end_profiling();

    // write runtime to file
    unsigned long long runtime = 0;
    for (const auto &kernel : profiling_data) {
        if (!kernel.second.runtimes.empty())
            runtime += kernel.second.min;
    }
    std::string runtime_file_name = config_file_dir;
    runtime_file_name.append("gemm_");
    runtime_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << (runtime / 1000000.0f);
    runtime_file.close();
}

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--mode",        1, false);
    args.addArgument("--platform-id", 1, false);
    args.addArgument("--device-id",   1, false);
    args.addArgument("--input-size",  3, false);
    args.addArgument("--alpha",       1, false);
    args.addArgument("--beta",        1, false);
    args.addArgument("--transpose-a", 1, false);
    args.addArgument("--transpose-b", 1, false);
    args.parse(static_cast<size_t>(argc), argv);
    std::string         mode        = args.retrieve_string("mode");
    size_t              platform_id = args.retrieve_size_t("platform-id");
    size_t              device_id   = args.retrieve_size_t("device-id");
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t M = input_size[0], N = input_size[1], K = input_size[2];
    const float alpha               = args.retrieve_float("alpha");
    const float beta                = args.retrieve_float("beta");
    const bool transpose_a          = args.retrieve_bool("transpose-a");
    const bool transpose_b          = args.retrieve_bool("transpose-b");

    // get device
    atf::cf::device_info device_info(static_cast<const int &>(platform_id), static_cast<const int &>(device_id));
    device_info.initialize();
    std::string device_type;
    cl_device_type type;
    atf::cf::check_error(device_info.device().getInfo(CL_DEVICE_TYPE, &type));
    switch (type) {
        case CL_DEVICE_TYPE_CPU:
            device_type = "cpu";
            break;
        case CL_DEVICE_TYPE_GPU:
            device_type = "gpu";
            break;
        case CL_DEVICE_TYPE_ACCELERATOR:
            device_type = "accelerator";
            break;
        case CL_DEVICE_TYPE_CUSTOM:
            device_type = "custom";
            break;
        default:
            std::cerr << "unknown device type." << std::endl;
            exit(EXIT_FAILURE);
    }


    // create context and command queue
    const auto &platform = device_info.platform();
    const auto &device = device_info.device();
    cl_context_properties props[] = {CL_CONTEXT_PLATFORM,
                                     reinterpret_cast<cl_context_properties>(platform()),
                                     0};

    cl::Context      context       = cl::Context(VECTOR_CLASS<cl::Device>(1, device), props);
    cl::CommandQueue command_queue = cl::CommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE);

    // prepare inputs
    std::vector<float> a(M * K); for (int i = 0; i < a.size(); ++i) a[i] = (i % 100) + 1;
    std::vector<float> b(K * N); for (int i = 0; i < b.size(); ++i) b[i] = (i % 100) + 1;
    std::vector<float> c(M * N); for (int i = 0; i < c.size(); ++i) c[i] = (i % 100) + 1;

    if (mode == "tune") {
        tune(platform_id, device_id, command_queue, device_type, M, N, K, transpose_a, transpose_b, a, b, c, alpha, beta);
    } else if (mode == "bench") {
        bench(platform_id, device_id, context, command_queue, device, device_type, M, N, K, transpose_a, transpose_b, a, b, c, alpha, beta);
    } else {
        std::cerr << "unknown mode! possible values: tune, bench" << std::endl;
        exit(EXIT_FAILURE);
    }
}