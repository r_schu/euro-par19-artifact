#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <mkl.h>
#include <fstream>

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--input-size",  3, false);
    args.addArgument("--alpha",       1, false);
    args.addArgument("--beta",        1, false);
    args.addArgument("--transpose-a", 1, false);
    args.addArgument("--transpose-b", 1, false);
    args.parse(static_cast<size_t>(argc), argv);
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t M = input_size[0], N = input_size[1], K = input_size[2];
    const float alpha               = args.retrieve_float("alpha");
    const float beta                = args.retrieve_float("beta");
    const bool transpose_a          = args.retrieve_bool("transpose-a");
    const bool transpose_b          = args.retrieve_bool("transpose-b");

    int ld_a = K, ld_b = N, ld_c = N;
    auto transpose_a_arg = CblasNoTrans;
    auto transpose_b_arg = CblasNoTrans;
    if (transpose_a) {
        transpose_a_arg = CblasTrans;
        ld_a = M;
    } else if (transpose_b) {
        transpose_b_arg = CblasTrans;
        ld_b = K;
    }

    // prepare inputs
    auto *a = (float *) mkl_malloc(M * K * sizeof(float), 64); for (int i = 0; i < M * K; ++i) a[i] = (i % 100) + 1;
    auto *b = (float *) mkl_malloc(K * N * sizeof(float), 64); for (int i = 0; i < K * N; ++i) b[i] = (i % 100) + 1;
    auto *c = (float *) mkl_malloc(M * N * sizeof(float), 64); for (int i = 0; i < M * N; ++i) c[i] = (i % 100) + 1;

    // warm ups
    std::cout << std::endl << "benchmarking Intel MKL..." << std::endl;
    double runtime = std::numeric_limits<double>::max();
    for (int i = 0; i < 10; ++i) {
        // time warm_ups to prevent compiler optimization
        auto start = dsecnd();
        cblas_sgemm(CblasRowMajor, transpose_a_arg, transpose_b_arg,
                    M, N, K,
                    alpha,
                    a, ld_a,
                    b, ld_b,
                    beta,
                    c, ld_c);
        auto end = dsecnd();
        runtime = std::min(runtime, (end - start) * 1000);
    }

    // evaluations
    runtime = std::numeric_limits<double>::max();
    for (int i = 0; i < 200; ++i) {
        auto start = dsecnd();
        cblas_sgemm(CblasRowMajor, transpose_a_arg, transpose_b_arg,
                    M, N, K,
                    alpha,
                    a, ld_a,
                    b, ld_b,
                    beta,
                    c, ld_c);
        auto end = dsecnd();
        runtime = std::min(runtime, (end - start) * 1000);
    }
    mkl_free(a);
    mkl_free(b);
    mkl_free(c);

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/cpu/mkl/");
    runtime_file_name.append("gemm_");
    runtime_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << runtime;
    runtime_file.close();
}