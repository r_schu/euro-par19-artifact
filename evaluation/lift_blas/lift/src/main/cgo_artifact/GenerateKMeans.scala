package cgo_artifact

import apart.arithmetic.{PerformSimplification, SizeVar}
import ir._
import ir.ast._
import opencl.executor.Compile
import opencl.generator.{PerformBarrierElimination, PerformLoopOptimisation}
import opencl.ir._
import opencl.ir.pattern._
import rewriting.utils.Utils

object GenerateKMeans {
  def main(args: Array[String]): Unit = {

    val P = SizeVar("P") // number of points
    val C = SizeVar("C") // number of clusters
    val F = SizeVar("F") // number of features

    val featuresType    = ArrayType(ArrayType(Float, P), F)
    val clustersType    = ArrayType(ArrayType(Float, F), C)

    val update = UserFun("update", Array("dist", "pair"),
      "{ return dist + (pair._0 - pair._1) * (pair._0 - pair._1); }",
      Seq(Float, TupleType(Float, Float)), Float)

    val test = UserFun("test", Array("dist", "tuple"),
      "{" +
        "float min_dist = tuple._0;" +
        "int i          = tuple._1;" +
        "int index      = tuple._2;" +
        "if (dist < min_dist) {" +
        "  Tuple t = {dist, i + 1, i};" +
        "  return t;" +
        "} else {" +
        "  Tuple t = {min_dist, i + 1, index};" +
        "  return t;" +
        "}" +
        "}",
      Seq(Float, TupleType(Float, Int, Int)), TupleType(Float, Int, Int))

    val select = UserFun("select_", Array("tuple"),
      "{ return tuple._2; }",
      Seq(TupleType(Float, Int, Int)), Int)

    val rodinia = fun(
      featuresType, clustersType,
      (features, clusters) => {
        features :>> Transpose() :>> MapGlb( \( feature => {
          clusters :>> ReduceSeq( \( (tuple, cluster) => {

            val dist = Zip(feature, cluster) :>> ReduceSeq(update, 0.0f )
            Zip(dist, tuple) :>> MapSeq(test)

          }), Value("{3.40282347e+38, 0, 0}", ArrayType(TupleType(Float, Int, Int), 1)) ) :>>
            toGlobal(MapSeq(MapSeq(select)))
        }) )
      })

    val code = Compile(rodinia, 256,1,1,P,1,1,collection.immutable.Map())

    val extension =
      if (PerformSimplification())
        ".cl"
      else if (!PerformLoopOptimisation() && !PerformBarrierElimination())
        "_no_opt.cl"
      else
        "_no_simpl.cl"

    Utils.dumpToFile(code, "kmeans" + extension, "generated_kernels/kmeans/")
  }
}
