package cgo_artifact

import apart.arithmetic.{PerformSimplification, SizeVar}
import ir._
import ir.ast._
import opencl.executor.Compile
import opencl.generator.{PerformBarrierElimination, PerformLoopOptimisation}
import opencl.ir._
import opencl.ir.pattern._
import rewriting.utils.Utils

object GenerateNBody {

  def main(args: Array[String]): Unit = {
    val calcAccAndAccumulate =
      UserFun("calcAcc", Array("p1", "p2", "deltaT", "espSqr", "acc"),
        """|{
          |  float4 r;
          |  r.xyz = p2.xyz - p1.xyz ;
          |  float distSqr = r.x*r.x + r.y*r.y + r.z*r.z;
          |  float invDist = 1.0f / sqrt(distSqr + espSqr);
          |  float invDistCube = invDist * invDist * invDist;
          |  float s = invDistCube * p2.w;
          |  float4 res;
          |  res.xyz = acc.xyz + s * r.xyz;
          |  return res;
          |}
          | """.stripMargin,
        Seq(Float4, Float4, Float, Float, Float4), Float4)


    val update =
      UserFun("update", Array("pos", "vel", "deltaT", "acceleration"),
        """|{
          |  float4 newPos;
          |  newPos.xyz = pos.xyz + vel.xyz * deltaT + 0.5f * acceleration.xyz * deltaT * deltaT;
          |  newPos.w = pos.w;
          |  float4 newVel;
          |  newVel.xyz = vel.xyz + acceleration.xyz * deltaT;
          |  newVel.w = vel.w;
          |  Tuple t = {newPos, newVel};
          |  return t;
          |}
        """.stripMargin,
        Seq(Float4, Float4, Float, Float4), TupleType(Float4, Float4))

    val N = SizeVar("N")

    val amd = fun(
      ArrayType(Float4, N),
      ArrayType(Float4, N),
      Float,
      Float,
      (pos, vel, espSqr, deltaT) =>
        MapGlb(fun(p1 =>

          toGlobal(MapSeq(fun(acceleration =>
            update(Get(p1, 0), Get(p1, 1), deltaT, acceleration))))

            o ReduceSeq(fun((acc, p2) =>
            calcAccAndAccumulate(Get(p1,0), p2, deltaT, espSqr, acc)),
            Value("(float4) 0.0f", Float4)) $ pos

        )) $ Zip(pos, vel)
    )

    val tileX = 256
    val tileY = 1

    val nvidia = fun(
      ArrayType(Float4, N),
      ArrayType(Float4, N),
      Float,
      Float,
      (pos, vel, espSqr, deltaT) =>
        Join() o
          MapWrg(1)(Join() o MapWrg(0)(fun(p1Chunk => // ArrayType(Flat4, tileX)
            \(newP1Chunk =>
              MapLcl(1)(\(bla =>
                toGlobal(MapLcl(0)( fun( p1 =>
                  update(Get(Get(p1,0), 0), Get(Get(p1, 0), 1), deltaT, Get(p1,1))
                ))) $ Zip(newP1Chunk, bla))) o
                Join() o
                ReduceSeq(fun((acc, p2) =>
                  Let(p2Local =>
                    MapLcl(1)(\(accDim2 =>
                      Join() o
                        MapLcl(0)(fun(p1 => // ( (float4, float4), float4 )

                          ReduceSeq(fun((acc, p2) =>
                            calcAccAndAccumulate(Get(Get(p1,0), 0), p2, deltaT, espSqr, acc)),
                            Get(p1,1)) $ accDim2._0
                        )) $ Zip(newP1Chunk, accDim2._1)
                    )) $ Zip(p2Local, acc)
                  ) o toLocal(MapLcl(1)(MapLcl(0)(idF4))) $ p2
                ), MapLcl(1)(MapLcl(0)(idF4)) $ Value("0.0f", ArrayType(ArrayType(Float4, tileX), tileY))) o Split(tileY) o Split(tileX) $ pos
            ) $ Zip(toPrivate(MapLcl(idF4)) $ Get(Unzip() $ p1Chunk, 0), Get(Unzip() $ p1Chunk, 1))
          )) o Split(tileX)) o Split(N) $ Zip(pos, vel)
    )

    val amdCode = Compile(amd, 128,1,1,N,1,1,collection.immutable.Map())
    val nvidiaCode = Compile(nvidia, 256,1,1,N,1,1,collection.immutable.Map())

    val extension =
      if (PerformSimplification())
        ".cl"
      else if (!PerformLoopOptimisation() && !PerformBarrierElimination())
        "_no_opt.cl"
      else
        "_no_simpl.cl"

   Utils.dumpToFile(amdCode, "nbody_amd" + extension, "generated_kernels/nbody_amd/")
   Utils.dumpToFile(nvidiaCode, "nbody_nvidia" + extension, "generated_kernels/nbody_nvidia")
  }

}
