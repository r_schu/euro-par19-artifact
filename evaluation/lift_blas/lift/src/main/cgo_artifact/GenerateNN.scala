package cgo_artifact

import apart.arithmetic.{PerformSimplification, SizeVar}
import ir._
import ir.ast._
import opencl.executor.Compile
import opencl.generator.{PerformBarrierElimination, PerformLoopOptimisation}
import opencl.ir._
import opencl.ir.pattern._
import rewriting.utils.Utils

object GenerateNN {
  def main(args: Array[String]): Unit = {

    val distance = UserFun("distance_", Array("loc", "lat", "lng"),
      "{ return sqrt( (lat - loc._0) * (lat - loc._0) + (lng - loc._1) * (lng - loc._1) ); }",
      Seq(TupleType(Float, Float), Float, Float), Float)

    val N = SizeVar("N")

    val nn = fun(
      ArrayType(TupleType(Float, Float), N), Float, Float,
      (locations, lat, lng) => {
        locations :>> MapGlb( \(loc => distance(loc, lat, lng)) )
      })

    val code = Compile(nn, 128,1,1,N,1,1,collection.immutable.Map())

    val extension =
      if (PerformSimplification())
        ".cl"
      else if (!PerformLoopOptimisation() && !PerformBarrierElimination())
        "_no_opt.cl"
      else
        "_no_simpl.cl"

    Utils.dumpToFile(code, "nn" + extension, "generated_kernels/nn/")
  }
}
