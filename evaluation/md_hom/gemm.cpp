#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <atf.h>
#include <json.hpp>

void tune(size_t platform_id, size_t device_id, atf::cf::device_info &device_info, const std::string &device_type,
          size_t M, size_t N, size_t K, bool transpose_a, bool transpose_b,
          const std::vector<float> &a, const std::vector<float> &b, const std::vector<float> &c,
          float alpha, float beta) {
    // get device information
    std::vector<size_t> max_wi_size;
    size_t max_wg_size = 0;
    device_info.get_device_limits(&max_wi_size, &max_wg_size);
    size_t combined_max_wi_size = 0;
    for (int i = 0; i < 3; ++i) {
        combined_max_wi_size = std::max(combined_max_wi_size, max_wi_size[i]);
    }

    std::string kernel_type;
    if (transpose_a)
        kernel_type += "T";
    else
        kernel_type += "N";
    if (transpose_b)
        kernel_type += "T";
    else
        kernel_type += "N";
    
    // define search space
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {1}, atf::less_than_or_eq(G_CB_RES_DEST_LEVEL));
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {0}, atf::less_than_or_eq(L_CB_RES_DEST_LEVEL));

    auto G_CB_SIZE_L_1       = atf::tp("G_CB_SIZE_L_1",       {int(M)});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          atf::interval<int>(1, M));
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          atf::interval<int>(1, std::min(M, combined_max_wi_size)), atf::less_than_or_eq((G_CB_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1));
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       atf::interval<int>(1, M), atf::equal((G_CB_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1));
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       {1});
    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {2});

    auto G_CB_SIZE_L_2       = atf::tp("G_CB_SIZE_L_2",       {int(N)});
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          atf::interval<int>(1, N));
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          atf::interval<int>(1, std::min(N, combined_max_wi_size)), atf::less_than_or_eq((G_CB_SIZE_L_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2));
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       atf::interval<int>(1, N), atf::equal((G_CB_SIZE_L_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2));
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       {1});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {1});

    auto G_CB_SIZE_R_1       = atf::tp("G_CB_SIZE_R_1",       {int(K)});
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          atf::interval<int>(1, K));
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          atf::interval<int>(1, std::min(K, combined_max_wi_size)), atf::less_than_or_eq((G_CB_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1));
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       atf::interval<int>(1, K), atf::equal((G_CB_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1));
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       {1});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0});

    // prepare tuner
    auto tuner = atf::open_tuner(new atf::cond::duration<std::chrono::hours>(5));
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_R_1)
            (G_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1)
            (G_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2)
            (G_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1);


    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        return config["NUM_WI_L_1"].value().int_val() <= max_wi_size[config["OCL_DIM_L_1"].value().int_val()] &&
               config["NUM_WI_L_2"].value().int_val() <= max_wi_size[config["OCL_DIM_L_2"].value().int_val()] &&
               config["NUM_WI_R_1"].value().int_val() <= max_wi_size[config["OCL_DIM_R_1"].value().int_val()] &&
               config["NUM_WI_L_1"].value().int_val() * config["NUM_WI_L_2"].value().int_val() * config["NUM_WI_R_1"].value().int_val() <= max_wg_size;
    };
    size_t res_g_l_size = M * N * sizeof(float);
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= std::min(config[NUM_WI_R_1.name()].value().int_val(), atf::ceil_lambda(std::min(config[L_CB_SIZE_R_1.name()].value().int_val(), std::min(config[NUM_WG_R_1.name()].value().int_val(), atf::ceil_lambda(config[G_CB_SIZE_R_1.name()].value().int_val(), config[L_CB_SIZE_R_1.name()].value().int_val()))), std::min(config[P_CB_SIZE_R_1.name()].value().int_val(), std::min(config[L_CB_SIZE_R_1.name()].value().int_val(), std::min(config[NUM_WG_R_1.name()].value().int_val(), atf::ceil_lambda(config[G_CB_SIZE_R_1.name()].value().int_val(), config[L_CB_SIZE_R_1.name()].value().int_val()))))));
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 2) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (NUM_WG_R_1) * (NUM_WI_R_1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 2) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (NUM_WI_R_1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 2) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1)))))))
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 2) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1)))))))
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::LOCAL,
            "../../extern/ATF/ocl_md_hom_process_wrapper_float",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "kernel/gemm_" + kernel_type + "_1.cl", "gemm_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(a), atf::buffer(b)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "kernel/gemm_" + kernel_type + "_2.cl", "gemm_2"},
            atf::inputs(atf::buffer(c)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // tune
    auto best_configuration = tuner(kernel);

    // store best found configuration if valid
    if (tuner.number_of_valid_evaluated_configs() > 0) {
        std::map<std::string, int> plain_config;
        for (const auto &tp : best_configuration) {
            plain_config[tp.first] = tp.second.value().int_val();
        }
        std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
        config_file_dir.append("/results/")
                .append(device_type).append("/")
                .append(std::to_string(platform_id)).append("/")
                .append(std::to_string(device_id)).append("/md_hom/");
        std::string config_file_name = config_file_dir;
        config_file_name.append("gemm_");
        config_file_name.append(std::to_string(M)).append("x")
                .append(std::to_string(N)).append("x")
                .append(std::to_string(K));
        config_file_name.append("_config.json");
        int ret = system(std::string("mkdir -p ").append(config_file_dir).c_str());
        if (ret != 0) {
            std::cerr << "Failed to create results directory." << std::endl;
            exit(EXIT_FAILURE);
        }
        std::ofstream config_file(config_file_name, std::ios::out | std::ios::trunc);
        config_file << std::setw(4) << nlohmann::json(plain_config);
        config_file.close();
    } else {
        std::cerr << "Tuning failed." << std::endl;
        exit(EXIT_FAILURE);
    }
}

void bench(size_t platform_id, size_t device_id, const atf::cf::device_info &device_info, const std::string &device_type,
           size_t M, size_t N, size_t K, bool transpose_a, bool transpose_b,
           const std::vector<float> &a, const std::vector<float> &b, const std::vector<float> &c,
           float alpha, float beta) {
    // read configuration
    std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
    config_file_dir.append("/results/")
            .append(device_type).append("/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id)).append("/md_hom/");
    std::string config_file_name = config_file_dir;
    config_file_name.append("gemm_");
    config_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    config_file_name.append("_config.json");
    std::ifstream config_file(config_file_name, std::ios::in);
    if (config_file.fail()) {
        std::cerr << "Unable to open configuration file. Has md_hom not yet been tuned for this input size?" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto plain_config = nlohmann::json::parse(config_file);

    std::string kernel_type;
    if (transpose_a)
        kernel_type += "T";
    else
        kernel_type += "N";
    if (transpose_b)
        kernel_type += "T";
    else
        kernel_type += "N";

    // set configuration for kernel execution
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {plain_config.at("CACHE_L_CB").get<int>()});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {plain_config.at("CACHE_P_CB").get<int>()});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {plain_config.at("G_CB_RES_DEST_LEVEL").get<int>()});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {plain_config.at("L_CB_RES_DEST_LEVEL").get<int>()});
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {plain_config.at("P_CB_RES_DEST_LEVEL").get<int>()});
    auto G_CB_SIZE_L_1      = atf::tp("G_CB_SIZE_L_1",      {plain_config.at("G_CB_SIZE_L_1").get<int>()});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       {plain_config.at("L_CB_SIZE_L_1").get<int>()});
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       {plain_config.at("P_CB_SIZE_L_1").get<int>()});
    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {plain_config.at("OCL_DIM_L_1").get<int>()});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          {plain_config.at("NUM_WG_L_1").get<int>()});
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          {plain_config.at("NUM_WI_L_1").get<int>()});
    auto G_CB_SIZE_L_2      = atf::tp("G_CB_SIZE_L_2",      {plain_config.at("G_CB_SIZE_L_2").get<int>()});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       {plain_config.at("L_CB_SIZE_L_2").get<int>()});
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       {plain_config.at("P_CB_SIZE_L_2").get<int>()});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {plain_config.at("OCL_DIM_L_2").get<int>()});
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          {plain_config.at("NUM_WG_L_2").get<int>()});
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          {plain_config.at("NUM_WI_L_2").get<int>()});
    auto G_CB_SIZE_R_1      = atf::tp("G_CB_SIZE_R_1",      {plain_config.at("G_CB_SIZE_R_1").get<int>()});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       {plain_config.at("L_CB_SIZE_R_1").get<int>()});
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       {plain_config.at("P_CB_SIZE_R_1").get<int>()});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {plain_config.at("OCL_DIM_R_1").get<int>()});
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          {plain_config.at("NUM_WG_R_1").get<int>()});
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          {plain_config.at("NUM_WI_R_1").get<int>()});
    auto tuner = atf::exhaustive();
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_R_1)
            (G_CB_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (G_CB_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (G_CB_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1);

    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    size_t res_g_l_size = M * N * sizeof(float);
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= std::min(config[NUM_WI_R_1.name()].value().int_val(), atf::ceil_lambda(std::min(config[L_CB_SIZE_R_1.name()].value().int_val(), std::min(config[NUM_WG_R_1.name()].value().int_val(), atf::ceil_lambda(config[G_CB_SIZE_R_1.name()].value().int_val(), config[L_CB_SIZE_R_1.name()].value().int_val()))), std::min(config[P_CB_SIZE_R_1.name()].value().int_val(), std::min(config[L_CB_SIZE_R_1.name()].value().int_val(), std::min(config[NUM_WG_R_1.name()].value().int_val(), atf::ceil_lambda(config[G_CB_SIZE_R_1.name()].value().int_val(), config[L_CB_SIZE_R_1.name()].value().int_val()))))));
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 2) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (NUM_WG_R_1) * (NUM_WI_R_1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 2) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (NUM_WI_R_1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 2) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1)))))))
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 0) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 1) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 2) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 2) * (NUM_WI_L_2)
            + (OCL_DIM_R_1 == 2) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1)))))))
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::NONE,
            "none",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "kernel/gemm_" + kernel_type + "_1.cl", "gemm_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(a), atf::buffer(b)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "kernel/gemm_" + kernel_type + "_2.cl", "gemm_2"},
            atf::inputs(atf::buffer(c)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            1, 0, // 0 warm ups and 1 evaluation because proper measurement is done below
            true,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // measure runtime
    std::cout << "measuring runtime... " << std::endl;
    std::cout.setstate(std::ios_base::failbit);
    auto best_config = tuner(kernel);
    std::cout.clear();
    kernel.warm_ups(10);
    kernel.evaluations(200);
    std::vector<std::vector<unsigned long long>> runtimes;
    kernel(best_config, &runtimes);

    // write runtime to file
    unsigned long long runtime = 0;
    for (const auto &times : runtimes) {
        if (!times.empty())
            runtime += *std::min_element(times.begin(), times.end());
    }
    std::string runtime_file_name = config_file_dir;
    runtime_file_name.append("gemm_");
    runtime_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << (runtime / 1000000.0f);
    runtime_file.close();
}

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--mode",        1, false);
    args.addArgument("--platform-id", 1, false);
    args.addArgument("--device-id",   1, false);
    args.addArgument("--input-size",  3, false);
    args.addArgument("--alpha",       1, false);
    args.addArgument("--beta",        1, false);
    args.addArgument("--transpose-a", 1, false);
    args.addArgument("--transpose-b", 1, false);
    args.parse(static_cast<size_t>(argc), argv);
    std::string         mode        = args.retrieve_string("mode");
    size_t              platform_id = args.retrieve_size_t("platform-id");
    size_t              device_id   = args.retrieve_size_t("device-id");
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t M = input_size[0], N = input_size[1], K = input_size[2];
    const float alpha               = args.retrieve_float("alpha");
    const float beta                = args.retrieve_float("beta");
    const bool transpose_a          = args.retrieve_bool("transpose-a");
    const bool transpose_b          = args.retrieve_bool("transpose-b");

    // get device
    atf::cf::device_info device_info(static_cast<const int &>(platform_id), static_cast<const int &>(device_id));
    device_info.initialize();
    std::string device_type;
    cl_device_type type;
    atf::cf::check_error(device_info.device().getInfo(CL_DEVICE_TYPE, &type));
    switch (type) {
        case CL_DEVICE_TYPE_CPU:
            device_type = "cpu";
            break;
        case CL_DEVICE_TYPE_GPU:
            device_type = "gpu";
            break;
        case CL_DEVICE_TYPE_ACCELERATOR:
            device_type = "accelerator";
            break;
        case CL_DEVICE_TYPE_CUSTOM:
            device_type = "custom";
            break;
        default:
            std::cerr << "unknown device type." << std::endl;
            exit(EXIT_FAILURE);
    }

    // prepare inputs
    std::vector<float> a(M * K); for (int i = 0; i < a.size(); ++i) a[i] = (i % 100) + 1;
    std::vector<float> b(K * N); for (int i = 0; i < b.size(); ++i) b[i] = (i % 100) + 1;
    std::vector<float> c(M * N); for (int i = 0; i < c.size(); ++i) c[i] = (i % 100) + 1;

    if (mode == "tune") {
        tune(platform_id, device_id, device_info, device_type, M, N, K, transpose_a, transpose_b, a, b, c, alpha, beta);
    } else if (mode == "bench") {
        bench(platform_id, device_id, device_info, device_type, M, N, K, transpose_a, transpose_b, a, b, c, alpha, beta);
    } else {
        std::cerr << "unknown mode! possible values: tune, bench" << std::endl;
        exit(EXIT_FAILURE);
    }
}