#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  rm -rf results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/* &> /dev/null
  mkdir -p results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/ &> /dev/null
  cp -r defaults/gpu/* results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/ &&
  printf "\n\nUsing gpu defaults. Note that performance might be suboptimal!\n"
} || {
  printf "\n\nFailed to use gpu defaults. This should not be happening!\n"
  exit 1
}