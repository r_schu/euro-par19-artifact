#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_CPU_PLATFORM_ID?"Please set the environment variable OCL_CPU_PLATFORM_ID."}
if [ -z "$OCL_CPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_CPU_DEVICE_ID?"Please set the environment variable OCL_CPU_DEVICE_ID."}
if [ -z "$OCL_CPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  # md_hom
  cd $ARTIFACT_ROOT/build/evaluation/md_hom &&
  mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom/

  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 64 10 500 --alpha 1 --beta 0 --transpose-a false --transpose-b true
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 64 500 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 50 500 64 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 50 64 500 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 50 64 1   --alpha 1 --beta 1 --transpose-a false --transpose-b false
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 500 64 50 --alpha 1 --beta 0 --transpose-a true  --transpose-b false
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 20 25 576 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 20 576 25 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 20 576 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  ./md_hom_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 64 2 10   --alpha 1 --beta 0 --transpose-a false --transpose-b true

  printf "\n\nmd_hom tuning successful!\n"
} || {
  printf "\n\nmd_hom tuning failed!\n"
  exit 1
}