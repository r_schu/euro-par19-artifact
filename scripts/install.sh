#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
mkdir build &> /dev/null || { rm -rf build; mkdir build; }
{
  # clBLAS
  cd $ARTIFACT_ROOT/build/ &&
  mkdir -p $ARTIFACT_ROOT/build/extern &&
  cp -r ../extern/clBLAS extern &&
  cd extern/clBLAS &&
  mkdir build &&
  mkdir install &&
  cd build &&
  cmake ${@:1} -DCMAKE_INSTALL_PREFIX=$ARTIFACT_ROOT/build/extern/clBLAS/install ../src &&
  make -j `nproc` install &&
  export clBLAS_DIR=${ARTIFACT_ROOT}/build/extern/clBLAS/install/lib64/cmake/clBLAS/ &&

  # initialize CMake project
  cd $ARTIFACT_ROOT/build &&
  cmake -DCMAKE_BUILD_TYPE="Release" ${@:1} .. &&
  make -j `nproc` &&

  # md_hom
  cp -r $ARTIFACT_ROOT/evaluation/md_hom/kernel $ARTIFACT_ROOT/build/evaluation/md_hom/ &&

  # Lift BLAS
  cp -r $ARTIFACT_ROOT/evaluation/lift_blas $ARTIFACT_ROOT/build/evaluation/ &&
  cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
  cd lift &&
  sbt compile &&
  cd .. &&
  ARTIFACT_ROOT=`pwd` APART_VECTOR_CAST=1 ./lift/scripts/GenerateMMNvidia &&

  printf "\n\nArtifact installation successful!\n"
} || {
  printf "\n\nArtifact installation failed!\n"
  exit 1
}