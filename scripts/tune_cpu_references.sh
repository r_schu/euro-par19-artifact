#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_CPU_PLATFORM_ID?"Please set the environment variable OCL_CPU_PLATFORM_ID."}
if [ -z "$OCL_CPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_CPU_DEVICE_ID?"Please set the environment variable OCL_CPU_DEVICE_ID."}
if [ -z "$OCL_CPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  # CLBlast
  cd ${ARTIFACT_ROOT}/build/evaluation/clblast
  mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_64x10x500_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 64 10 500 --alpha 1 --beta 0 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_64x500x1_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 64 500 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_50x500x64_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 50 500 64 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_50x64x500_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 50 64 500 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_50x64x1_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 50 64 1   --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_500x64x50_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 500 64 50 --alpha 1 --beta 0 --transpose-a true  --transpose-b false
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_20x25x576_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 20 25 576 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_20x576x25_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 20 576 25 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_20x576x1_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 20 576 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/clblast/gemm_64x2x10_config.json &> /dev/null
  ./clblast_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 64 2 10   --alpha 1 --beta 0 --transpose-a false --transpose-b true

  # clBLAS
  cd ${ARTIFACT_ROOT}/build/extern/clBLAS/install/bin &&
  mkdir -p $ARTIFACT_ROOT/build/extern/clBLAS/install/storage &&
  export CLBLAS_STORAGE_PATH=$ARTIFACT_ROOT/build/extern/clBLAS/install/storage &&
  export CLBLAS_PLATFORM_ID=$OCL_CPU_PLATFORM_ID &&
  export CLBLAS_DEVICE_ID=$OCL_CPU_DEVICE_ID &&
  ./clBLAS-tune --gemm --float --store-kernels &&

  printf "\n\nReference tuning successful!\n"
} || {
  printf "\n\nReference tuning failed!\n"
  exit 1
}