#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi
: ${CUDA_GPU_DEVICE_ID?"Please set the environment variable CUDA_GPU_DEVICE_ID."}
if [ -z "$CUDA_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable CUDA_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please set the environment variable ARTIFACT_ROOT to the root dir of the artifact (the directory containing the scripts folder)."; exit 1; }
{
  # NVIDIA cuBLAS
  cd $ARTIFACT_ROOT/build/evaluation/cublas &&
  mkdir -p ${ARTIFACT_ROOT}/results/gpu/cublas
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 64 10 500 --alpha 1 --beta 0 --transpose-a false --transpose-b true
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 64 500 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 50 500 64 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 50 64 500 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 50 64 1   --alpha 1 --beta 1 --transpose-a false --transpose-b false
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 500 64 50 --alpha 1 --beta 0 --transpose-a true  --transpose-b false
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 20 25 576 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 20 576 25 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 20 576 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size 64 2 10   --alpha 1 --beta 0 --transpose-a false --transpose-b true

  # Lift BLAS
  cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
  (
    export PARENT_ARTIFACT_ROOT=$ARTIFACT_ROOT
    export ARTIFACT_ROOT=`pwd`
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 64 -s 10 -s 500 > gemm_lift_64x10x500.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_64x10x500.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_64x10x500_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 64 -s 500 -s 1 > gemm_lift_64x500x1.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_64x500x1.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_64x500x1_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 50 -s 500 -s 64 > gemm_lift_50x500x64.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_50x500x64.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_50x500x64_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 50 -s 64 -s 500 > gemm_lift_50x64x500.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_50x64x500.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_50x64x500_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 50 -s 64 -s 1 > gemm_lift_50x64x1.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_50x64x1.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_50x64x1_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 500 -s 64 -s 50 > gemm_lift_500x64x50.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_500x64x50.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_500x64x50_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 20 -s 25 -s 576 > gemm_lift_20x25x576.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_20x25x576.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_20x25x576_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 20 -s 576 -s 25 > gemm_lift_20x576x25.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_20x576x25.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_20x576x25_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 20 -s 576 -s 1 > gemm_lift_20x576x1.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_20x576x1.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_20x576x1_runtime
    }
    {
      lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s 64 -s 2 -s 10 > gemm_lift_64x2x10.log &&
      mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
      grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_64x2x10.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_64x2x10_runtime
    }
  )

  # CLBlast
  cd ${ARTIFACT_ROOT}/build/evaluation/clblast
  mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_64x10x500_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 64 10 500 --alpha 1 --beta 0 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_64x500x1_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 64 500 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_50x500x64_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 50 500 64 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_50x64x500_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 50 64 500 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_50x64x1_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 50 64 1   --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_500x64x50_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 500 64 50 --alpha 1 --beta 0 --transpose-a true  --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_20x25x576_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 20 25 576 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_20x576x25_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 20 576 25 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_20x576x1_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 20 576 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblast/gemm_64x2x10_runtime &> /dev/null
  ./clblast_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 64 2 10   --alpha 1 --beta 0 --transpose-a false --transpose-b true

  # clBLAS
  cd ${ARTIFACT_ROOT}/build/evaluation/clblas
  mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_64x10x500_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 64 10 500 --alpha 1 --beta 0 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_64x500x1_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 64 500 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_50x500x64_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 50 500 64 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_50x64x500_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 50 64 500 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_50x64x1_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 50 64 1   --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_500x64x50_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 500 64 50 --alpha 1 --beta 0 --transpose-a true  --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_20x25x576_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 20 25 576 --alpha 1 --beta 1 --transpose-a false --transpose-b true
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_20x576x25_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 20 576 25 --alpha 1 --beta 0 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_20x576x1_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 20 576 1  --alpha 1 --beta 1 --transpose-a false --transpose-b false
  rm $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/clblas/gemm_64x2x10_runtime &> /dev/null
  ./clblas_gemm --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size 64 2 10   --alpha 1 --beta 0 --transpose-a false --transpose-b true

  printf "\n\nReference execution successful!\n"
} || {
  printf "\n\nReference execution failed!\n"
  exit 1
}