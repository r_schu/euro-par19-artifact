import sys
import glob
import os
import re
from os import listdir
from os.path import isdir, join
from tabulate import tabulate

results_dir = sys.argv[1]

def build_map(path, application):
    runtimes = {}
    for r in glob.glob("{0}/{1}_*_runtime".format(path, application)):
        file_name = os.path.split(r)[-1]
        size = re.match("{0}_(.*)_runtime".format(application), file_name).group(1)
        with open(r, "r") as file:
            runtimes[size] = float(file.read())
    return runtimes


# CPU
if isdir("{0}/cpu".format(results_dir)):
    mkl_gemm = build_map("{0}/cpu/mkl/".format(results_dir), "gemm")

    for platform_id in [f for f in listdir("{0}/cpu/".format(results_dir)) if isdir(join("{0}/cpu/".format(results_dir), f))]:
        if not re.match("^[0-9]+$", platform_id):
            continue
        for device_id in [f for f in listdir("{0}/cpu/{1}".format(results_dir, platform_id)) if isdir(join("{0}/cpu/{1}".format(results_dir, platform_id), f))]:
            if not re.match("^[0-9]+$", device_id):
                continue

            clblast_dir = "{0}/cpu/{1}/{2}/clblast".format(results_dir, platform_id, device_id)
            clblast_gemm = build_map(clblast_dir, "gemm")

            clblas_dir = "{0}/cpu/{1}/{2}/clblas".format(results_dir, platform_id, device_id)
            clblas_gemm = build_map(clblas_dir, "gemm")

            lift_dir = "{0}/cpu/{1}/{2}/lift".format(results_dir, platform_id, device_id)
            lift_gemm = build_map(lift_dir, "gemm")

            md_hom_dir = "{0}/cpu/{1}/{2}/md_hom".format(results_dir, platform_id, device_id)
            md_hom_gemm = build_map(md_hom_dir, "gemm")

            print
            print
            print "=== CPU (Platform: {0}, Device: {1}) ===".format(platform_id, device_id)
            print

            print "--- GEMM: Speedup of our implementation over the references ---"
            data = []
            for key in md_hom_gemm:
                md_hom_runtime = md_hom_gemm[key]
                speedup_clblast = None
                if key in clblast_gemm:
                    speedup_clblast = clblast_gemm[key] / md_hom_runtime
                speedup_clblas = None
                if key in clblas_gemm:
                    speedup_clblas = clblas_gemm[key] / md_hom_runtime
                speedup_lift = None
                if key in lift_gemm:
                    speedup_lift = lift_gemm[key] / md_hom_runtime
                speedup_mkl = None
                if key in mkl_gemm:
                    speedup_mkl = mkl_gemm[key] / md_hom_runtime
                data.append([key, speedup_clblast, speedup_clblas, speedup_lift, speedup_mkl])
            print tabulate(data, headers=["input size", "CLBlast", "clBLAS", "Lift", "Intel MKL"])
            print


# GPU
if isdir("{0}/gpu".format(results_dir)):
    cublas_gemm = build_map("{0}/gpu/cublas/".format(results_dir), "gemm")

    for platform_id in [f for f in listdir("{0}/gpu/".format(results_dir)) if isdir(join("{0}/gpu/".format(results_dir), f))]:
        if not re.match("^[0-9]+$", platform_id):
            continue
        for device_id in [f for f in listdir("{0}/gpu/{1}".format(results_dir, platform_id)) if isdir(join("{0}/gpu/{1}".format(results_dir, platform_id), f))]:
            if not re.match("^[0-9]+$", device_id):
                continue

            clblast_dir = "{0}/gpu/{1}/{2}/clblast".format(results_dir, platform_id, device_id)
            clblast_gemm = build_map(clblast_dir, "gemm")

            clblas_dir = "{0}/gpu/{1}/{2}/clblas".format(results_dir, platform_id, device_id)
            clblas_gemm = build_map(clblas_dir, "gemm")

            lift_dir = "{0}/gpu/{1}/{2}/lift".format(results_dir, platform_id, device_id)
            lift_gemm = build_map(lift_dir, "gemm")

            md_hom_dir = "{0}/gpu/{1}/{2}/md_hom".format(results_dir, platform_id, device_id)
            md_hom_gemm = build_map(md_hom_dir, "gemm")

            print
            print
            print "=== GPU (Platform: {0}, Device: {1}) ===".format(platform_id, device_id)
            print

            print "--- GEMM: Speedup of our implementation over the references ---"
            data = []
            for key in md_hom_gemm:
                md_hom_runtime = md_hom_gemm[key]
                speedup_clblast = None
                if key in clblast_gemm:
                    speedup_clblast = clblast_gemm[key] / md_hom_runtime
                speedup_clblas = None
                if key in clblas_gemm:
                    speedup_clblas = clblas_gemm[key] / md_hom_runtime
                speedup_lift = None
                if key in lift_gemm:
                    speedup_lift = lift_gemm[key] / md_hom_runtime
                speedup_cublas = None
                if key in cublas_gemm:
                    speedup_cublas = cublas_gemm[key] / md_hom_runtime
                data.append([key, speedup_clblast, speedup_clblas, speedup_lift, speedup_cublas])
            print tabulate(data, headers=["input size", "CLBlast", "clBLAS", "Lift", "NVIDIA cuBLAS"])
            print