//
// Created by Richard Schulze on 31.01.18.
//

#include <dlfcn.h>
#include <iostream>
#include "timer.hpp"
#include "clpp11.hpp"

bool _logging_acive = false;
unsigned short int _warm_ups = 0;
unsigned short int _evaluations = 1;
std::unordered_map<std::string, std::vector<unsigned long long>> _runtimes;

namespace clblast {
void RunKernel(Kernel &kernel, Queue &queue, const Device &device,
               std::vector <size_t> global, const std::vector <size_t> &local,
               EventPointer event, const std::vector <Event> &waitForEvents) {
    typedef void (*signature)(Kernel &, Queue &, const Device &,
                              std::vector<size_t>, const std::vector<size_t>&,
                              EventPointer, const std::vector <Event> &);
    static signature original_method = nullptr;
    if (original_method == nullptr) {
#if 0
        // Mali
        void *tmpPtr = dlsym(RTLD_NEXT, "_ZN7clblast9RunKernelERNS_6KernelERNS_5QueueERKNS_6DeviceESt6vectorIjSaIjEERKS9_PP9_cl_eventRKS7_INS_5EventESaISF_EE");
#else
        void *tmpPtr = dlsym(RTLD_NEXT, "_ZN7clblast9RunKernelERNS_6KernelERNS_5QueueERKNS_6DeviceESt6vectorImSaImEERKS9_PP9_cl_eventRKS7_INS_5EventESaISF_EE");
#endif
        if (tmpPtr == nullptr) {
            std::cerr << "RunKernel method could not be found" << std::endl;
            auto *error = dlerror();
            if (error) {
                std::cerr << error << std::endl;
            }
            exit(EXIT_FAILURE);
        }
        memcpy(&original_method, &tmpPtr, sizeof(void*));
    }
    if (_logging_acive) {
        auto timing_event = event;
        if (event == nullptr) {
            timing_event = new cl_event;
        }
        for (int i = 0; i < _warm_ups; ++i) {
            (*original_method)(kernel, queue, device, global, local, timing_event, waitForEvents);
            clWaitForEvents(1, timing_event);
        }
        for (int i = 0; i < _evaluations; ++i) {
            (*original_method)(kernel, queue, device, global, local, timing_event, waitForEvents);
            clWaitForEvents(1, timing_event);
            cl_ulong start, end;
            clGetEventProfilingInfo(*timing_event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, nullptr);
            clGetEventProfilingInfo(*timing_event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, nullptr);
            _runtimes[kernel.GetFunctionName()].push_back(end - start);
        }
        if (event == nullptr) {
            clReleaseEvent(*timing_event);
            delete timing_event;
        }
    } else {
        (*original_method)(kernel, queue, device, global, local, event, waitForEvents);
    }
}
}

void start_profiling(unsigned short int warm_ups, unsigned short int evaluations) {
    _warm_ups = warm_ups;
    _evaluations = evaluations;
    _runtimes.clear();
    _logging_acive = true;
}

std::unordered_map<std::string, struct util::profiling_info> end_profiling() {
    _logging_acive = false;
    std::unordered_map<std::string, struct util::profiling_info> infos;
    for (const auto &kernel_runtimes : _runtimes) {
        auto info = util::make_profiling_info(_warm_ups, _evaluations, kernel_runtimes.second);
        infos[kernel_runtimes.first] = info;
    }
    _runtimes.clear();
    return infos;
}