//
//  differential_evolution.hpp
//  new_atf_lib
//

#ifndef differential_evolution_h
#define differential_evolution_h

#include <random>

#include "tuner_with_constraints.hpp"
#include "tuner_without_constraints.hpp"
#include "coordinate_space.hpp"

#ifdef EVAL
#include <cctof.hpp>
#endif

/** number of vectors of the population with a minimum of 4 */
#define NUM_VECTORS     30
/** number of vectors to create the donor vector */
#define NUM_MUT_VECTORS 3
/** mutation factor which scale the donor vector */
#define F_VAL               0.7      // 0.4 - 1.0
/** Crossover-Rate to combine the donor vector with the current vector */
#define CR              0.9      // 0.0 - 1.0
/** number of variants to calculate the donor vector */
#define NUM_VARIANTS    2
/** number of retries to calculate a new trial vector if the current one isn't in the search space */
#define INVALID_RETRIES 1

namespace atf
{
    /**
    * @brief Implementation of the differential evolution search technique.
    *
    * @tparam T  type of tuner to inherit from
    */
    template< typename T = tuner_with_constraints>
    class differential_evolution_class : public T
    {
    public:
        /**
         * @brief   Constructs new differential evolution technique.
         *
         * @tparam Ts       types of forward-arguments for tuner base class
         * @param params    forward-arguments for tuner base class
         */
        template< typename... Ts >
        differential_evolution_class( Ts... params )
                : T(  params... ),
                  _donor_variant(1)
        {}

        /**
         * @brief   Sets the variant to calculate the donor vector. Default 1.
         *
         * @param number   number that set the variant
         */
        void setDonorVar(int number){
            if(number >= 1 || number <= NUM_VARIANTS)
                _donor_variant = number;
        }

        /**
         * @brief   Initializes an initial population and the search space where is the search technique is working on.
         *
         * Initializes the trial vector with an random point.
         * Initializes the search space as coordinate space.
         *
         * @tparam search_space  search_space to initialize the coordinate space
         */
        void initialize( const search_space& search_space )
        {
#ifdef EVAL
            cctof::scoped_timer<cctof::search_timer> t;
#endif
            _search_space = coordinate_space{search_space};

            _generator = std::default_random_engine(random_seed() );
            _current_vec = 0;

            population_init();
            _trial_vector = _search_space.random_point();
        }

        /**
         * @brief   Calculate the next trial vector for the next configuration.
         *
         * @return  returns the trial vector or the initial vector (first generation)
         */
        configuration get_next_config()
        {
#ifdef EVAL
            cctof::scoped_timer<cctof::search_timer> t;
#endif
            if(_population_costs.at(_current_vec) == -1){
                return _search_space.get_config(_vector_population.at(_current_vec));
            } else {
                setTrialVector();
                return _search_space.get_config(_trial_vector);
            }
        }

        /**
         * @brief   Reported the calculated result of the last trial vector.
         *
         * Sets the trial vector instead of the current vector if his costs are smaller or equal
         * than the costs from the current vector.
         * Sets a new initial vector if the current initial vector is an invalid configuration.
         *
         * @param result   cost of the last trial vector
         */
        void report_result( const unsigned long long& result )
        {
#ifdef EVAL
            cctof::search_timer<> t;
            t.start();
#endif
            if(_population_costs.at(_current_vec) == -1){
                if(result == std::numeric_limits<unsigned long long>::max()){
                    _vector_population.at(_current_vec) = _search_space.random_point();
                }
                else
                    _population_costs.at(_current_vec) = result;
            }
            else if(result <= _population_costs.at(_current_vec)){
                _vector_population.at(_current_vec) = _trial_vector;
                _population_costs.at(_current_vec) = result;
            }

            if(_current_vec < NUM_VECTORS -1)
                _current_vec++;
            else{
                _current_vec = 0;           //Next generation
            }
#ifdef EVAL
            t.stop();
            cctof::timetable::get() += t;
#endif
        }

        /**
         * @brief   Finalizes the differential evolution search technique.
         */
        void finalize()
        {}

    private:
        /** search space */
        coordinate_space                    _search_space;
        /** generator to determine random values */
        std::default_random_engine          _generator;
        /** vector of points from population */
        std::vector<point>                  _vector_population;
        /** trial vector */
        point                               _trial_vector;
        /** vector of costs from each point of population */
        std::vector<int>                    _population_costs;
        /** counter of the vectors of population */
        size_t                              _current_vec;
        /** number of donor variants */
        int                                 _donor_variant;

        /**
         * @brief   Calculates a random seed based on the system time.
         *
         * @return  returns the seed
         */
        unsigned int random_seed() const
        {
            return static_cast<unsigned int>( std::chrono::system_clock::now().time_since_epoch().count() );
        }

        /**
         * @brief   Initializes the population.
         *
         * Initializes the population with random points and the costs with -1.
         */
        void population_init()
        {
            for(int i = 0; i < NUM_VECTORS; i++) {
                _vector_population.push_back(_search_space.random_point());
                _population_costs.push_back(-1);
            }
        }

        /**
         * @brief   Selects randomly number of NUM_MUT_VECTORS distinct points.
         *
         * @return  returns an array with the indices from the selected points
         */
        int* random_vectors() {
            std::uniform_int_distribution<int>  mutation_distribution = std::uniform_int_distribution<int>( 0, static_cast<int>(NUM_VECTORS -1) );
            int *vecs = new int[NUM_MUT_VECTORS];
            for(int i = 0; i < NUM_MUT_VECTORS; i++){
                vecs[i] = mutation_distribution(_generator);
                for(int j = 0; j<i; j++){
                    if( (vecs[i] == vecs[j] && i != j) || vecs[i] == _current_vec){
                        vecs[i] = mutation_distribution(_generator);
                        j=-1;
                    }
                }
            }
            return vecs;
        }

        /**
         * @brief Sets the trial vector for the current vector.
         *
         * Set the parameter i from the trial vector to the parameter from the donor vector, if the random value (0-1) is <= CR
         * or if the parameter i == random value (0-dimension) (-> this value make sure that trial vector and current vector differ).
         * Else set the parameter i from the trial vector to the parameter from the current vector.
         */
        void setTrialVector(){
            int random_param, *mutation_vec_indizes,loop_count=0;
            size_t dimension = _trial_vector.dimension();
            std::uniform_int_distribution<int> recombi_distribution = std::uniform_int_distribution<int>(0, static_cast<int>(dimension - 1));
            std::uniform_real_distribution<double> cr_distribution = std::uniform_real_distribution<double>(0,static_cast<double>( 1.0 ));

            do {
                loop_count++;
                random_param = recombi_distribution(_generator);
                mutation_vec_indizes = random_vectors();

                for (int i = 0; i < dimension; i++) {
                    if (cr_distribution(_generator) <= CR || i == random_param)
                        _trial_vector.replace(i, getDonorVector(i, mutation_vec_indizes));
                    else
                        _trial_vector.replace(i, _vector_population.at(_current_vec)[i]);
                }

            } while (! _search_space.is_valid_point(_trial_vector) && loop_count < INVALID_RETRIES);
            if(! _search_space.is_valid_point(_trial_vector))
                _trial_vector = _search_space.convert_to_valid_point_mod(_trial_vector);
        }

        /**
         * @brief   Sets the donor vector for the given parameter.
         *
         * Variant 1: x_rand1[param] + F_VAL * (x_rand2[param] - x_rand3[param])
         * Variant 2: x_best[param] + F_VAL * (x_rand2[param] - x_rand3[param])
         *
         * @param param                 Indice of parameter to calculate
         * @param mutation_vec_indizes  array of indices from the points that are needed to calculate the donor vector
         *
         * @return  parameter of the donor vector
         */
        double getDonorVector (int param, int *mutation_vec_indizes){
            //std::uniform_real_distribution<double> F_distribution = std::uniform_real_distribution<double>(0.5,static_cast<double>( 1.0 ));
            //double F = F_distribution(_generator);

            switch (_donor_variant){
                case 1:
                    return _vector_population.at(mutation_vec_indizes[0])[param]
                           + F_VAL * (_vector_population.at(mutation_vec_indizes[1])[param] -
                                  _vector_population.at(mutation_vec_indizes[2])[param]);
                case 2:
                    std::vector<int>::iterator result = std::min_element(std::begin(_population_costs), std::end(_population_costs));
                    int best = std::distance(std::begin(_population_costs), result);
                    return _vector_population.at(_current_vec)[param] +
                           F_VAL * (_vector_population.at(best)[param] - _vector_population.at(_current_vec)[param]) +
                           F_VAL * (_vector_population.at(mutation_vec_indizes[0])[param] -
                                _vector_population.at(mutation_vec_indizes[1])[param]);
            }
            return 0;
        }

        std::string display_string() const {
            return "Differential Evolution";
        }

    };

    /**
     * @brief         Constructs a new instance of differential evolution-class.
     *
     * @tparam Ts     types to forward to tuner base class
     * @param args    arguments to forward to tuner base class
     *
     * @return        new instance of differential evolution search technique
     */
    template< typename... Ts >
    auto differential_evolution( Ts... args )
    {
        return differential_evolution_class<>{ args... };
    }

    /**
     * @brief         Constructs a new instance of differential evolution-class, derived from specified tuner.
     *
     * @tparam Ts     types to forward to tuner base class
     * @param args    arguments to forward to tuner base class
     *
     * @return        new instance of differential evolution search technique
     */
    template< typename T, typename... Ts >
    auto differential_evolution( Ts... args )
    {
        return differential_evolution_class<T>{ args... };
    }


} // namespace "atf"

#endif /* differential_evolution_h */