/**
 * \file    simulated_annealing_opentuner.hpp
 * \date    03.08.18.
 */

#ifndef simulated_annealing_opentuner_h
#define simulated_annealing_opentuner_h

#include <map>
#include <utility>
#include <random>
#include <cmath>
#include <limits>

#include "tuner_with_constraints.hpp"
#include "tuner_without_constraints.hpp"
#include "coordinate_space.hpp"

#ifdef EVAL
#include <cctof.hpp>
#endif

namespace atf
{
    /**
     * \brief Implementation of the ATF-interface for a simulated annealing technique inspired by OpenTuner
     *
     * \tparam T      type of tuner to inherit from
     */
    template< typename T = tuner_with_constraints>
    class simulated_annealing_opentuner_class : public T
    {
    public:
        template< typename... Ts >
        simulated_annealing_opentuner_class( Ts... params )
                : T(  params... )
        {}

        /**
         * \brief Initializes the simulated annealing for the search space it works on.
         *
         * Simulated annealing uses the coordinate space to work with, an abstraction of the ATF default search space.
         * Initializes the search space, the time counter and sets the first state to "INITIALIZATION". Also creates via
         * interpolation the temperature shedule and depending on it the maximum time for cooling.
         *
         * \param search_space      search space to initialize the simulated annealing for
         */
        void initialize( const search_space& search_space )
        {
#ifdef EVAL
            cctof::scoped_timer<cctof::search_timer> t;
#endif
            _search_space = coordinate_space {search_space};
            _current_state = INITIALIZATION;
            _time = 0;
            while(interp_steps.size() < temps.size() - 1)
            {
                interp_steps.push_back(_default_interp_steps);
            }

            for(size_t t = 0; t < temps.size() - 1; ++t)
            {
                for(int steps = interp_steps[t]; steps > 0; --steps)
                {
                    _schedule.push_back(interp(temps[t+1], temps[t], static_cast<float>(steps)/interp_steps[t]));
                }
            }
            _schedule.push_back(temps.back());
            _max_time = static_cast<int>(_schedule.size()) - 1;
        }

        /**
         * \brief Provides the next configuration.
         *
         * Calculates the next potential points for the neighbourhood. For every parameter of the current point, if the
         * parameter value is in the interval (0, 1) two copies of that point are created. One is mutated
         * with addition and the other one with subtraction.
         * The maximum range of possible parameter changes is set in _step_size depending on current temperature and time.
         *
         * \return Returns the next configuration to check.
         */
        configuration get_next_config()
        {
#ifdef EVAL
            cctof::scoped_timer<cctof::search_timer> t;
#endif
            switch(_current_state) {
                case INITIALIZATION: {
                    _current_parameter = 0;
                    _temp = _schedule.at(std::min(_time, _max_time));
                    _step_size = get_step_size(_time, _temp);
                    _current_point = _search_space.random_point();
                    _neighbours.push_back(std::make_pair(_current_point,0));
                    return _search_space.get_config(_neighbours.front().first);
                }
                case EXPLORE_PLUS: {
                    if(_current_point[_current_parameter] < 1.0f) {
                        _neighbours.push_back(std::make_pair(_current_point,0));
                        _neighbours.back().first[_current_parameter] += _step_size*random();
                        if(_current_point[_current_parameter] <= 0.0f) {
                            _current_state = EXPLORE_MINUS;
                        }
                        return _search_space.get_config(_neighbours.back().first);
                    }
                    _current_state = EXPLORE_MINUS;
                    _neighbours.push_back(std::make_pair(_current_point,0));
                    _neighbours.back().first[_current_parameter] -= _step_size*random();
                    return _search_space.get_config(_neighbours.back().first);
                }
                case EXPLORE_MINUS: {
                    _neighbours.push_back(std::make_pair(_current_point,0));
                    _neighbours.back().first[_current_parameter] -= _step_size*random();
                    return _search_space.get_config(_neighbours.back().first);
                }
                default:
                    throw std::runtime_error( "Invalid algorithm state" );
            }
        }

        /**
         * \brief Takes the determined result and saves it in the corresponding pair. If neighbourhood is complete,
         * iterates through it and determines next configuration to go on with via AcceptanceFunction.
         *
         * Adds the corresponding costs to the in get_next_config() created pair. If the neighbourhood ist complete it
         * determines the next configuration by iterating randomly through the neighbours and checking its acceptance via
         * the AcceptanceFunction, going on with the found configuration or the best found configuration if no neighbour
         * was good enough to find a new set of neighbours.
         *
         * \param result        the runtime costs of the last checked configuration
         */
        void report_result( const unsigned long long& result )
        {
#ifdef EVAL
            cctof::search_timer<> t;
	    t.start();
#endif
            switch(_current_state) {
                case INITIALIZATION: {
                    _neighbours.front().second = result;
                    _best_result = result;
                    _current_state = EXPLORE_PLUS;
                    break;
                }
                case EXPLORE_PLUS: {
                    _neighbours.back().second = result;
                    if(_neighbours.back().second < _best_result) {
                        _best_point = _neighbours.back().first;
                        _best_result = _neighbours.back().second;
                    }
                    _current_state = EXPLORE_MINUS;
                    break;
                }
                case EXPLORE_MINUS: {
                    _neighbours.back().second = result;
                    if(_neighbours.back().second < _best_result) {
                        _best_point = _neighbours.back().first;
                        _best_result = _neighbours.back().second;
                    }
                    ++_current_parameter;
                    if(_current_parameter == _current_point.dimension()) {
                        _current_parameter = 0;
                        unsigned long long current_result;
                           while(true) {
                               if(_neighbours.empty()) {
                                   _current_point = _best_point;
                                   current_result = _best_result;
                                   break;
                               }
                               std::uniform_int_distribution<> uid{0, static_cast<int>(_neighbours.size()) - 1};
                               int candidate = uid(_dre);
                               if (random() < AcceptanceFunction(1.0f ,
                                                                 relative(static_cast<float>(_neighbours.at(candidate).second),
                                                                          static_cast<float>(_best_result)),
                                                                 _temp)) {
                                   _current_point = _neighbours.at(candidate).first;
                                   current_result = _neighbours.at(candidate).second;
                                   break;
                               }
                               _neighbours.erase(_neighbours.begin() + candidate);
                           }
                           ++_time;
                           if(_time > _max_time) {
                               _time = _time - _max_time;
                           }
                           _temp = _schedule.at(std::min(_time, _max_time));
                           _step_size = get_step_size(_time, _temp);
                           _neighbours.clear();
                           _neighbours.push_back(std::make_pair(_current_point,current_result));
                           _current_state = EXPLORE_PLUS;
                    }
                    else {
                        _current_state = EXPLORE_PLUS;
                    }
                    break;
                }
                default:
                    throw std::runtime_error( "Invalid algorithm state" );
            }
#ifdef EVAL
      t.stop();
      cctof::timetable::get() += t;
#endif
        }

        /**
         * \brief Finalizes the simulated annealing search technique.
         */
        void finalize()
        {}


    private:
        /** holds the default number of steps to interpolate */
        const int _default_interp_steps = 100;
        /** holds a list of temperatures to interpolate between */
        const std::vector<float> temps = {30, 0};
        /** holds a set of numbers that determine how to interpolate between temps elements */
        std::vector<int> interp_steps;

        /** state to indicate what to do next */
        enum State{
            INITIALIZATION,
            EXPLORE_PLUS,
            EXPLORE_MINUS
        };
        /** coordinate search space */
        coordinate_space                        _search_space;
        /** current state */
        State                                   _current_state;
        /** number of determined next configurations/time */
        int                                     _time;
        /** maximum number of configurations within this cooling period/maximum cooling time */
        int                                     _max_time;
        /** indicates the current parameter to mutate*/
        size_t                                  _current_parameter;
        /** holds the current best result found */
        unsigned long long                                  _best_result;
        /** holds the current temperature */
        float                                   _temp;
        /** holds the current step size range */
        float                                   _step_size;
        /** holds the current point */
        point                                   _current_point;
        /** holds the best point found yet */
        point                                   _best_point;
        /** the temperature schedule */
        std::vector<float>                      _schedule;
        /** vector that holds all potentially next points */
        std::vector<std::pair<point, unsigned long long>>   _neighbours;
        /**  the random engine and distribution */
        std::default_random_engine _dre{std::random_device()()};
        std::uniform_real_distribution<float>   _urd{0.f,1.f};


        /**
         * \brief Generates a random float in the intervall [0,1).
         *
         * @return Returns a random float.
         */
        float random()
        {
            return _urd(_dre);
        }


        /**
         * \brief Calculates one step of an interpolation.
         *
         * \param a     lower interpolate boundary
         * \param b     upper interpolate boundary
         * \param t     step depending multiplier
         *
         * \return Returns the value for the step of the interpolation.
         */
        float interp(float a, float b, float t)
        {
            if(t < 0.f || t > 1.f)
            {
                throw std::invalid_argument{"t has to be in [0,1]"};
            }
            return a + t*(b-a);
        }


        /**
         * \brief Calculates the new step size.
         *
         * Function from the simulated annealing implementation in OpenTuner to calculate the next step size.
         *
         * \param time      counter that determines what the current temperature is
         * \param temp      current temperature
         *
         * \return Returns the new step size.
         */
        float get_step_size(int time, float temp)
        {
            return exp(-(20.0f + static_cast<float>(time)/100.0f)/(temp + 1.0f));
        }


        /**
         * \brief Acceptance probablity function from the simulated annealing implementation in OpenTuner.
         *
         * If costs of the best configuration are better than the ones from candidate, the candidate is accepted with
         * probability defined by exp(50*(1-relative(candidate/best))/temp) adapted from Boltzmann distribution.
         */
        float AcceptanceFunction(float e, float e_new, float temp)
        {
            if(e >= e_new) {
                return 1.f;
            }
            if(temp == 0) {
                return 0.f;
            }
            if(50*(e_new-e)/temp > 10) {
                return 0.f;
            }
            return static_cast<float>(exp(50.0f*(e-e_new)/temp));
        }


        /**
         * \brief Relates the given results and returns the parameter for the acceptance function.
         *
         * Function from the simulated annealing implementation in OpenTuner to calculate the speedup of result2
         * to result 1.
         *
         * \param result1   first result
         * \param result2   second result
         *
         * \return Returns the speedup for the acceptance function.
         */
        float relative(float result1, float result2)
        {
            if(result2 == 0) {
                return result1 * std::numeric_limits<float>::infinity();
            }
            return result1/result2;
        }
    };


    /**
     * \brief Construct a new instance of the simulated annealing search technique.
     *
     * \tparam Ts     types to forward to tuner base class
     * \param args    arguments to forward to tuner base class
     *
     * \return Returns a new instance of the simulated annealing search technique.
     */
    template< typename... Ts >
    auto simulated_annealing_opentuner( Ts... args )
    {
        return simulated_annealing_opentuner_class<>{ args... };
    }


    /**
     * \brief Construct a new instance of the simulated annealing search technique, derived from specified tuner.
     *
     * \tparam Ts     types to forward to tuner base class
     * \param args    arguments to forward to tuner base class
     *
     * \return Returns a new instance of the simulated annealing search technique.
     */
    template< typename T, typename... Ts >
    auto simulated_annealing_opentuner( Ts... args )
    {
        return simulated_annealing_opentuner_class<T>{ args... };
    }

} // namespace "atf"

#endif /* simulated_annealing_opentuner_h */
