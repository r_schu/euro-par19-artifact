//
//  round_robin.hpp
//  new_atf_lib
//

#ifndef round_robin_hpp
#define round_robin_hpp

#include "tuner_with_constraints.hpp"
#include "tuner_without_constraints.hpp"

namespace atf
{

template< typename T = tuner_with_constraints>
class round_robin_class : public T
{
  public:
    template< typename... Ts >
    round_robin_class( std::vector<std::reference_wrapper<T>> techniques, Ts... params )
      : T(  params... ), _techniques( techniques ), _current_technique_index( 0 )
    {}


    void initialize( const search_space& search_space )
    {
      for( auto& technique : _techniques )
        technique.get().initialize( search_space );
    }
  
  
    configuration get_next_config()
    {
      return _techniques[ _current_technique_index ].get().get_next_config();
    }
  
    
    void report_result( const unsigned long long& result )
    {
      _techniques[ _current_technique_index ].get().report_result( result );
      _current_technique_index = ( _current_technique_index + 1 ) % _techniques.size();
    }
  
  
    void finalize()
    {
      for( auto& technique : _techniques )
        technique.get().finalize();
    }

    std::string display_string() const {
        return "Round Robin";
    }

  private:

    std::vector<std::reference_wrapper<T>> _techniques;
    size_t                                 _current_technique_index;
};


template< typename... Ts >
auto round_robin( std::vector<std::reference_wrapper<atf::tuner_with_constraints>> techniques, Ts... args )
{
  return round_robin_class<>{ techniques, args... };
}


template< typename T, typename... Ts >
auto round_robin( std::vector<std::reference_wrapper<T>> techniques, Ts... args )
{
  return round_robin_class<T>{ techniques, args... };
}


} // namespace "atf"



#endif /* round_robin_hpp */
