//
// Created by   on 07.12.17.
//
#include <argparse.hpp>
#include <helper.hpp>
#include "ocl_md_hom_wrapper.hpp"

//#define DEBUG

#if BUFFER_TYPE_ID == 3
typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
} Pixel;
std::istream& operator>>(std::istream& is, Pixel& p) {
    int r, g, b;
    is >> r >> g >> b;
    p.r = r;
    p.g = g;
    p.b = b;
    return is;
}
std::ostream& operator<<(std::ostream& os, const Pixel& p) {
    os << p.r << " " << p.g << " " << p.b;
    return os;
}
#endif

std::vector<BUFFER_TYPE> _expected_result;
void load_expected_result(const std::string &file) {
    std::ifstream is(file);
    std::istream_iterator<BUFFER_TYPE> start(is), end;
    _expected_result = std::vector<BUFFER_TYPE>(start, end);
}

int _socket = 0;
void connect_to_server(const std::string &server_type, const std::string &server, unsigned short int port) {
    _socket = socket(server_type == "local" ? AF_UNIX : AF_INET, SOCK_STREAM, 0);
    if (_socket < 0) {
        std::cerr << "error while creating socket: " << strerror(errno) << std::endl;
        exit(1);
    }

    atf::cf::addr server_addr{};
    if (server_type == "local") {
        server_addr.addr_un.sun_family = AF_UNIX;
        strncpy(server_addr.addr_un.sun_path, server.c_str(), sizeof(server_addr.addr_un.sun_path) - 1);
    } else {
        memset(&server_addr.addr_in, 0, sizeof(server_addr.addr_in));
        server_addr.addr_in.sin_family = AF_INET;
        server_addr.addr_in.sin_port = htons(port);

        struct hostent *hostinfo;
        hostinfo = gethostbyname(server.c_str());
        if (hostinfo == nullptr)
        {
            std::cerr << "invalid server: " << server << std::endl;
            exit(1);
        }
        server_addr.addr_in.sin_addr = *(struct in_addr *) hostinfo->h_addr;
    }

    struct sockaddr* addr_ptr = nullptr;
    if (server_type == "local") {
        addr_ptr = (struct sockaddr*) &server_addr.addr_un;
    } else {
        addr_ptr = (struct sockaddr*) &server_addr.addr_in;
    }
    if (connect(_socket, addr_ptr, server_type == "local" ? sizeof(struct sockaddr_un) : sizeof(struct sockaddr_in)) < 0) {
        std::cerr << "error while connecting to server: " << strerror(errno) << std::endl;
        exit(1);
    }
}

int main(int argc, const char **argv) {
    if (argc == 7 && std::string(argv[1]) == "device_info") {
        const int platform_id = std::stoi(argv[2]);
        const int device_id = std::stoi(argv[3]);
        const std::string server_type = argv[4];
        const std::string server = argv[5];
        const unsigned short int port = std::stoi(argv[6]);

        std::vector<cl::Platform> platforms;
        auto ocl_error = cl::Platform::get(&platforms);
        atf::cf::check_error(ocl_error);
        if (platform_id >= platforms.size()) {
            std::cerr << "Platform with id " << platform_id << " not found." << std::endl;
            exit(EXIT_FAILURE);
        }
        std::string platform_name;
        platforms[platform_id].getInfo(CL_PLATFORM_VENDOR, &platform_name);
        std::vector<cl::Device> devices;
        platforms[platform_id].getDevices(CL_DEVICE_TYPE_ALL, &devices);
        if (device_id >= devices.size()) {
            std::cerr << "Device with id " << device_id << " not found in platform with name "
                      << platform_name << "." << std::endl;
            exit(EXIT_FAILURE);
        }
        cl::Device device = devices[device_id];
        cl_uint dims;
        atf::cf::check_error(device.getInfo(CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, &dims));
        std::vector<size_t> max_num_wi(dims);
        atf::cf::check_error(device.getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, &max_num_wi));
        size_t max_wg_size;
        atf::cf::check_error(device.getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, &max_wg_size));
        cl_ulong max_local_mem;
        atf::cf::check_error(device.getInfo(CL_DEVICE_LOCAL_MEM_SIZE, &max_local_mem));
        std::string device_name;
        atf::cf::check_error(device.getInfo(CL_DEVICE_NAME, &device_name));
        for (auto &ch : device_name)
            if (ch == 0) ch = ' ';
        device_name.erase(device_name.begin(), std::find_if(device_name.begin(), device_name.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        device_name.erase(std::find_if(device_name.rbegin(), device_name.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), device_name.end());

        // connect to server
        connect_to_server(server_type, server, port);

        // send device name
        {
            size_t val = device_name.length();
            __uint64_t val_64 = htobe64(val);
            if (send(_socket, &val_64, sizeof(val_64), 0) < 0) {
                std::cerr << "error while sending device name length: " << strerror(errno) << std::endl;
                exit(1);
            }
            if (send(_socket, device_name.c_str(), device_name.length(), 0) < 0) {
                std::cerr << "error while sending device name: " << strerror(errno) << std::endl;
                exit(1);
            }
        }

        // send WI dimension count
        {
            size_t val = max_num_wi.size();
            __uint64_t val_64 = htobe64(val);
            if (send(_socket, &val_64, sizeof(val_64), 0) < 0) {
                std::cerr << "error while sending WI dimension count: " << strerror(errno) << std::endl;
                exit(1);
            }
        }

        // send WI dimension sizes
        for (const auto &val : max_num_wi) {
            __uint64_t val_64 = htobe64(val);
            if (send(_socket, &val_64, sizeof(val_64), 0) < 0) {
                std::cerr << "error while sending WI dimension sizes: " << strerror(errno) << std::endl;
                exit(1);
            }
        }

        // send max WG size
        {
            size_t val = max_wg_size;
            __uint64_t val_64 = htobe64(val);
            if (send(_socket, &val_64, sizeof(val_64), 0) < 0) {
                std::cerr << "error while sending max WG size: " << strerror(errno) << std::endl;
                exit(1);
            }
        }

        // send max local memory size
        {
            size_t val = max_local_mem;
            __uint64_t val_64 = htobe64(val);
            if (send(_socket, &val_64, sizeof(val_64), 0) < 0) {
                std::cerr << "error while sending max local memory size: " << strerror(errno) << std::endl;
                exit(1);
            }
        }

        // close connection
        close(_socket);

        return EXIT_SUCCESS;
    }

    // define arguments
    ArgumentParser args;
    args.appName("ocl_md_hom_process_wrapper");
    args.addArgument("--platform-id",               1,   false);
    args.addArgument("--device-id",                 1,   false);
    args.addArgument("--routine-name",              2,   false);
    args.addArgument("--source-file",               2,   false);
    args.addArgument("--flags",                     1,   false);

    args.addArgument("--kernel-1-input-sizes",      '*', false);
    args.addArgument("--kernel-1-res-g-size",       1,   false);
    args.addArgument("--kernel-1-result-size",      1,   false);
    args.addArgument("--kernel-1-global-size",      3,   false);
    args.addArgument("--kernel-1-local-size",       3,   false);

    args.addArgument("--needs-kernel-2",            1,   false);
    args.addArgument("--kernel-2-res-g-size",       1,   false);
    args.addArgument("--kernel-2-input-sizes",      '*', false);
    args.addArgument("--kernel-2-global-size",      3,   false);
    args.addArgument("--kernel-2-local-size",       3,   false);

    args.addArgument("--server-type",               1,   false);
    args.addArgument("--server",                    1,   false);
    args.addArgument("--port",                      1,   false);
    args.addArgument("--warm-ups",                  1,   false);
    args.addArgument("--evaluations",               1,   false);
    args.addArgument("--warm-up-timeout",           1,   false);
    args.addArgument("--evaluation-timeout",        1,   false);
    args.addArgument("--expected-result-file",      1,   false);

    // if benchmark flag is set, all evaluation runtimes will be send back to the tuning process
    args.addArgument("--benchmark",                 1,   false);

    args.addArgument("--buffer-fill-algorithm",     1,   false);

    // parse arguments
    args.parse(static_cast<size_t>(argc), argv);
    const int                      platform_id               = args.retrieve_int                      ("platform-id");
    const int                      device_id                 = args.retrieve_int                      ("device-id");
    const std::vector<std::string> routine_name              = args.retrieve_string_vector            ("routine-name");
    const std::vector<std::string> source_file               = args.retrieve_string_vector            ("source-file");
    const std::string              flags                     = args.retrieve_string                   ("flags");

    const std::vector<std::string> kernel_1_input_sizes      = args.retrieve_string_vector            ("kernel-1-input-sizes");
    const size_t                   kernel_1_res_g_size       = args.retrieve_size_t                   ("kernel-1-res-g-size");
    const size_t                   kernel_1_result_size      = args.retrieve_size_t                   ("kernel-1-result-size");
    const std::vector<size_t>      kernel_1_global_size      = args.retrieve_size_t_vector            ("kernel-1-global-size");
    const std::vector<size_t>      kernel_1_local_size       = args.retrieve_size_t_vector            ("kernel-1-local-size");

    const bool                     needs_kernel_2            = args.retrieve_bool                     ("needs-kernel-2");
    const size_t                   kernel_2_res_g_size       = args.retrieve_size_t                   ("kernel-2-res-g-size");
    const std::vector<std::string> kernel_2_input_sizes      = args.retrieve_string_vector            ("kernel-2-input-sizes");
    const std::vector<size_t>      kernel_2_global_size      = args.retrieve_size_t_vector            ("kernel-2-global-size");
    const std::vector<size_t>      kernel_2_local_size       = args.retrieve_size_t_vector            ("kernel-2-local-size");

    const std::string              server_type               = args.retrieve_string                   ("server-type");
    if (server_type != "local" && server_type != "remote") {
        std::cerr << "unknown server type: " << server_type << std::endl;
        exit(1);
    }
    const std::string              server                    = args.retrieve_string                   ("server");
    const unsigned short int       port                      = args.retrieve_unsigned_short_int       ("port");
    const size_t                   evaluations               = args.retrieve_size_t                   ("evaluations");
    const size_t                   warm_ups                  = args.retrieve_size_t                   ("warm-ups");
    const unsigned long long       warm_up_timeout           = args.retrieve_unsigned_long_long       ("warm-up-timeout");
    const unsigned long long       evaluation_timeout        = args.retrieve_unsigned_long_long       ("evaluation-timeout");
    const std::string              expected_result_file      = args.retrieve_string                   ("expected-result-file");
    const bool                     check_result              = !expected_result_file.empty() && expected_result_file != "none";

    const bool                     benchmark                 = args.retrieve_bool                     ("benchmark");
    const std::string              buffer_fill_algorithm     = args.retrieve_string                   ("buffer-fill-algorithm");

    // load expected result in background thread
    std::unique_ptr<std::thread> loading_thread;
    if (check_result) {
        loading_thread = std::make_unique<std::thread>(std::thread(load_expected_result, expected_result_file));
    }

    // set up buffer
    std::vector<atf::runtime_input> kernel_1_inputs;
    std::vector<BUFFER_TYPE> kernel_1_result(kernel_1_result_size);
    std::vector<atf::runtime_input> kernel_2_inputs;
    atf::INPUT_TYPE mode =
#if BUFFER_TYPE_ID == 0
            // float
            (buffer_fill_algorithm == "linear" ? atf::LINEAR_BUFFER_FLOAT : atf::WRAP_BUFFER_FLOAT)
#elif BUFFER_TYPE_ID == 1
            // double
            (buffer_fill_algorithm == "linear" ? atf::LINEAR_BUFFER_DOUBLE : atf::WRAP_BUFFER_DOUBLE)
#elif BUFFER_TYPE_ID == 2
            // size_t
            (buffer_fill_algorithm == "linear" ? atf::LINEAR_BUFFER_SIZE_T : atf::WRAP_BUFFER_SIZE_T)
#elif BUFFER_TYPE_ID == 3
            // Pixel (uses size_t for input buffers)
            (buffer_fill_algorithm == "linear" ? atf::LINEAR_BUFFER_SIZE_T : atf::WRAP_BUFFER_SIZE_T)
#endif
;
    for (const auto &input_size : kernel_1_input_sizes) {
        if (input_size == "lbf") mode = atf::LINEAR_BUFFER_FLOAT;
        else if (input_size == "lbd") mode = atf::LINEAR_BUFFER_DOUBLE;
        else if (input_size == "lbst") mode = atf::LINEAR_BUFFER_SIZE_T;
        else if (input_size == "wbf") mode = atf::WRAP_BUFFER_FLOAT;
        else if (input_size == "wbd") mode = atf::WRAP_BUFFER_DOUBLE;
        else if (input_size == "wbst") mode = atf::WRAP_BUFFER_SIZE_T;
        else if (input_size == "wbmccf33") mode = atf::WRAP_BUFFER_WEIGHTS_F_3_3;
        else if (input_size == "wbmccf333") mode = atf::WRAP_BUFFER_WEIGHTS_F_3_3_3;
        else if (input_size == "wbmccf55") mode = atf::WRAP_BUFFER_WEIGHTS_F_5_5;
        else if (input_size == "si") mode = atf::SCALAR_INT;
        else if (input_size == "sf") mode = atf::SCALAR_FLOAT;
        else if (input_size == "sd") mode = atf::SCALAR_DOUBLE;
        else if (input_size == "sst") mode = atf::SCALAR_SIZE_T;
        else {
            atf::runtime_input tmp = {mode, input_size};
            kernel_1_inputs.push_back(tmp);
        }
    }
    mode =
#if BUFFER_TYPE_ID == 0
            // float
            (buffer_fill_algorithm == "linear" ? atf::LINEAR_BUFFER_FLOAT : atf::WRAP_BUFFER_FLOAT)
#elif BUFFER_TYPE_ID == 1
            // double
            (buffer_fill_algorithm == "linear" ? atf::LINEAR_BUFFER_DOUBLE : atf::WRAP_BUFFER_DOUBLE)
#elif BUFFER_TYPE_ID == 2
            // size_t
            (buffer_fill_algorithm == "linear" ? atf::LINEAR_BUFFER_SIZE_T : atf::WRAP_BUFFER_SIZE_T)
#elif BUFFER_TYPE_ID == 3
            // Pixel (uses size_t for input buffers)
            (buffer_fill_algorithm == "linear" ? atf::LINEAR_BUFFER_SIZE_T : atf::WRAP_BUFFER_SIZE_T)
#endif
            ;
    for (const auto &input_size : kernel_2_input_sizes) {
        if (input_size == "lbf") mode = atf::LINEAR_BUFFER_FLOAT;
        else if (input_size == "lbd") mode = atf::LINEAR_BUFFER_DOUBLE;
        else if (input_size == "lbst") mode = atf::LINEAR_BUFFER_SIZE_T;
        else if (input_size == "wbf") mode = atf::WRAP_BUFFER_FLOAT;
        else if (input_size == "wbd") mode = atf::WRAP_BUFFER_DOUBLE;
        else if (input_size == "wbst") mode = atf::WRAP_BUFFER_SIZE_T;
        else if (input_size == "wbmccf33") mode = atf::WRAP_BUFFER_WEIGHTS_F_3_3;
        else if (input_size == "wbmccf333") mode = atf::WRAP_BUFFER_WEIGHTS_F_3_3_3;
        else if (input_size == "wbmccf55") mode = atf::WRAP_BUFFER_WEIGHTS_F_5_5;
        else if (input_size == "si") mode = atf::SCALAR_INT;
        else if (input_size == "sf") mode = atf::SCALAR_FLOAT;
        else if (input_size == "sd") mode = atf::SCALAR_DOUBLE;
        else if (input_size == "sst") mode = atf::SCALAR_SIZE_T;
        else {
            atf::runtime_input tmp = {mode, input_size};
            kernel_2_inputs.push_back(tmp);
        }
    }

    // set up kernel wrapper
    auto res_g_size = [&](unsigned int kernel, atf::configuration &config) -> size_t {
        return (kernel == 1 ? kernel_1_res_g_size : kernel_2_res_g_size) * sizeof(BUFFER_TYPE);
    };
    auto int_res_size = [&](atf::configuration &config) -> size_t {
        return kernel_1_result_size * sizeof(BUFFER_TYPE);
    };
    auto needs_second_kernel_lambda = [&](atf::configuration &config) -> bool {
        return needs_kernel_2;
    };
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    atf::cf::timeout_value warm_up_timeout_value{.absolute = warm_up_timeout};
    atf::cf::timeout_value evaluation_timeout_value{.absolute = evaluation_timeout};
    atf::cf::device_info device_info(platform_id, device_id);
    device_info.initialize(true);
    auto wrapper = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, source_file[0], routine_name[0], flags},
            atf::inputs(kernel_1_inputs),
            atf::cf::GS(kernel_1_global_size[0], kernel_1_global_size[1], kernel_1_global_size[2]),
            atf::cf::LS(kernel_1_local_size[0],  kernel_1_local_size[1],  kernel_1_local_size[2]),
            {atf::cf::kernel_info::FILENAME, source_file[1], routine_name[1], flags},
            atf::inputs(kernel_2_inputs),
            atf::cf::GS(kernel_2_global_size[0], kernel_2_global_size[1], kernel_2_global_size[2]),
            atf::cf::LS(kernel_2_local_size[0],  kernel_2_local_size[1],  kernel_2_local_size[2]),
            res_g_size,
            atf::buffer(kernel_1_result),
            int_res_size,
            needs_second_kernel_lambda,
            is_valid,
            evaluations, warm_ups,
            true,
            {atf::cf::NONE, "", ""},
            {atf::cf::ABSOLUTE, warm_up_timeout_value},
            {atf::cf::ABSOLUTE, evaluation_timeout_value}
    );

    // set up result check
    if (check_result) {
        // wait for expected result to be loaded
        loading_thread->join();
        wrapper.check_result(_expected_result.data());
    }

    // create dummy configuration with flags
    atf::configuration config;
    std::string val;
    config["FLAGS"] = atf::tp_value("FLAGS", atf::value_type(flags), &val);

    // measure runtime
    int error_code = 0;
    unsigned long long compile_time = 0;
    unsigned long long runtime;
    std::vector<std::vector<unsigned long long>> runtimes;
    bool send_compile_time = true;
    bool send_error_code = true;
    bool send_runtimes = true;
    try {
        runtime = wrapper(config, benchmark ? &runtimes : nullptr, &compile_time, &error_code);
    } catch (const cl::Error &err) {
        send_runtimes = false;
        error_code = err.err();
        std::cerr << "what(): " << err.what() << std::endl
                  << "err(): " << err.err() << std::endl;
    } catch (...) {
        send_runtimes = false;
    }

#ifndef DEBUG
    // connect to server
    connect_to_server(server_type, server, port);

    // notify server what values to receive
    uint16_t send_mask = 0;
    if (send_compile_time) send_mask |= 1;
    if (send_error_code) send_mask |= 2;
    if (send_runtimes) send_mask |= 4;
    send_mask = htons(send_mask);
    if (send(_socket, &send_mask, sizeof(send_mask), 0) < 0) {
        std::cerr << "error while sending send_mask: " << strerror(errno) << std::endl;
        exit(1);
    }

    if (send_compile_time) {
        // send compile time
        __uint64_t compile_time_64 = htobe64(compile_time);
        if (send(_socket, &compile_time_64, sizeof(compile_time_64), 0) < 0) {
            std::cerr << "error while sending compile_time: " << strerror(errno) << std::endl;
            exit(1);
        }
    }

    if (send_error_code) {
        uint32_t error_code_32 = htonl(static_cast<uint32_t>(std::abs(error_code)));
        if (send(_socket, &error_code_32, sizeof(error_code_32), 0) < 0) {
            std::cerr << "error while sending absolute error code: " << strerror(errno) << std::endl;
            exit(1);
        }
        uint32_t error_sign_32 = error_code < 0 ? htonl(1) : htonl(0);
        if (send(_socket, &error_sign_32, sizeof(error_sign_32), 0) < 0) {
            std::cerr << "error while sending error code sign: " << strerror(errno) << std::endl;
            exit(1);
        }
    }

    if (send_runtimes) {
        // send runtime
        __uint64_t runtime_64 = htobe64(runtime);
        if (send(_socket, &runtime_64, sizeof(runtime_64), 0) < 0) {
            std::cerr << "error while sending runtime: " << strerror(errno) << std::endl;
            exit(1);
        }

        if (benchmark) {
            // send all runtimes, if benchmark
            for (const auto &kernel_runtimes : runtimes) {
                // send kernel runtimes
                for (auto rt : kernel_runtimes) {
                    runtime_64 = htobe64(rt);
                    if (send(_socket, &runtime_64, sizeof(runtime_64), 0) < 0) {
                        std::cerr << "error while sending runtime: " << strerror(errno) << std::endl;
                        exit(1);
                    }
                }
            }
        }
    }

    // close connection
    close(_socket);
#endif

    return 0;
}