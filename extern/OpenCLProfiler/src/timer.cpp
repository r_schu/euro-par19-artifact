//
// Created by Richard Schulze on 31.01.18.
//

#include <CL/cl.h>
#include <dlfcn.h>
#include <iostream>
#include "timer.hpp"

bool _logging_acive = false;
unsigned short int _warm_ups = 0;
unsigned short int _evaluations = 1;
std::unordered_map<std::string, std::vector<unsigned long long>> _runtimes;

//extern "C" cl_program clCreateProgramWithSource(cl_context context,
//                                                cl_uint count,
//                                                const char **strings,
//                                                const size_t *lengths,
//                                                cl_int *errcode_ret) {
//    typedef cl_program (*signature)(cl_context,
//                                    cl_uint,
//                                    const char **,
//                                    const size_t *,
//                                    cl_int *);
//    static signature original_method = nullptr;
//    if (original_method == nullptr) {
//        void *tmpPtr = dlsym(RTLD_NEXT, "clCreateProgramWithSource");
//        memcpy(&original_method, &tmpPtr, sizeof(void*));
//    }
//    for (cl_uint i = 0; i < count; ++i) {
//        std::cout << std::endl << std::endl;
//        for (size_t c = 0; c < lengths[i]; ++c) {
//            std::cout << strings[i][c];
//        }
//    }
//    return (*original_method)(context, count, strings, lengths, errcode_ret);
//}

extern "C" cl_int clEnqueueNDRangeKernel(cl_command_queue command_queue,
                                         cl_kernel        kernel,
                                         cl_uint          work_dim,
                                         const size_t *   global_work_offset,
                                         const size_t *   global_work_size,
                                         const size_t *   local_work_size,
                                         cl_uint          num_events_in_wait_list,
                                         const cl_event * event_wait_list,
                                         cl_event *       event) {
    typedef cl_int (*signature)(cl_command_queue,
                                cl_kernel,
                                cl_uint,
                                const size_t *,
                                const size_t *,
                                const size_t *,
                                cl_uint,
                                const cl_event *,
                                cl_event *);
    static signature original_method = nullptr;
    if (original_method == nullptr) {
        void *tmpPtr = dlsym(RTLD_NEXT, "clEnqueueNDRangeKernel");
        memcpy(&original_method, &tmpPtr, sizeof(void*));
    }
    if (_logging_acive) {
//        std::cout << "global size: " << global_work_size[0] << "\t" << global_work_size[1] << "\t" << global_work_size[2] << std::endl;
//        std::cout << " local size: " << local_work_size[0] << "\t" << local_work_size[1] << "\t" << local_work_size[2] << std::endl;

        size_t kernel_name_length;
        clGetKernelInfo(kernel, CL_KERNEL_FUNCTION_NAME, sizeof(kernel_name_length), nullptr, &kernel_name_length);
        char *kernel_name_cstr = new char[kernel_name_length + 1];
        clGetKernelInfo(kernel, CL_KERNEL_FUNCTION_NAME, kernel_name_length * sizeof(char), kernel_name_cstr, nullptr);
        kernel_name_cstr[kernel_name_length] = '\0';
        std::string kernel_name = kernel_name_cstr;
        delete[] kernel_name_cstr;

        auto timing_event = event;
        if (event == nullptr) {
            timing_event = new cl_event;
        }
        for (int i = 0; i < _warm_ups; ++i) {
            (*original_method)(command_queue,
                               kernel,
                               work_dim,
                               global_work_offset,
                               global_work_size,
                               local_work_size,
                               num_events_in_wait_list,
                               event_wait_list, timing_event);
            clWaitForEvents(1, timing_event);
        }
        for (int i = 0; i < _evaluations; ++i) {
            (*original_method)(command_queue,
                               kernel,
                               work_dim,
                               global_work_offset,
                               global_work_size,
                               local_work_size,
                               num_events_in_wait_list,
                               event_wait_list, timing_event);
            clWaitForEvents(1, timing_event);
            cl_ulong start, end;
            clGetEventProfilingInfo(*timing_event, CL_PROFILING_COMMAND_START, sizeof(cl_ulong), &start, nullptr);
            clGetEventProfilingInfo(*timing_event, CL_PROFILING_COMMAND_END, sizeof(cl_ulong), &end, nullptr);
            _runtimes[kernel_name].push_back(end - start);
        }
        if (event == nullptr) {
            clReleaseEvent(*timing_event);
            delete timing_event;
        }
    }
    return (*original_method)(command_queue,
                              kernel,
                              work_dim,
                              global_work_offset,
                              global_work_size,
                              local_work_size,
                              num_events_in_wait_list,
                              event_wait_list, event);
}

void start_profiling(unsigned short int warm_ups, unsigned short int evaluations) {
    _warm_ups = warm_ups;
    _evaluations = evaluations;
    _runtimes.clear();
    _logging_acive = true;
}

std::unordered_map<std::string, struct util::profiling_info> end_profiling() {
    _logging_acive = false;
    std::unordered_map<std::string, struct util::profiling_info> infos;
    for (const auto &kernel_runtimes : _runtimes) {
        auto info = util::make_profiling_info(_warm_ups, _evaluations, kernel_runtimes.second);
        infos[kernel_runtimes.first] = info;
    }
    _runtimes.clear();
    return infos;
}