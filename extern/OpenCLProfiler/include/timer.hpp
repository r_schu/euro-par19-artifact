//
// Created by Richard Schulze on 31.01.18.
//

#ifndef OCL_TIMER_HPP
#define OCL_TIMER_HPP

#include <profiling.hpp>
#include <unordered_map>

void start_profiling(unsigned short int warm_ups, unsigned short int evaluations);

std::unordered_map<std::string, struct util::profiling_info> end_profiling();

#endif //OCL_TIMER_HPP
