//
// Created by Richard Schulze on 03.02.18.
//

#ifndef META_HPP
#define META_HPP

#define STR(x) #x
#define STR_E(x) STR(x)

#if defined(__clang__)
/* Clang/LLVM. ---------------------------------------------- */
#define COMPILER_NAME "Clang/LLVM"
#define COMPILER_VERSION STR_E(__clang_major__) "." STR_E(__clang_minor__) "." STR_E(__clang_patchlevel__)

#elif defined(__ICC) || defined(__INTEL_COMPILER)
/* Intel ICC/ICPC. ------------------------------------------ */
#define COMPILER_NAME "Intel ICC/ICPC"
#error "not implemented"
#elif defined(__GNUC__) || defined(__GNUG__)
/* GNU GCC/G++. --------------------------------------------- */
#define COMPILER_NAME "GNU GCC/G++"
#ifdef __GNUC_PATCHLEVEL__
#define COMPILER_VERSION STR_E(__GNUC__) "." STR_E(__GNUC_MINOR__) "." STR_E(__GNUC_PATCHLEVEL__)
#else
#define COMPILER_VERSION STR_E(__GNUC__) "." STR_E(__GNUC_MINOR__) ".x"
#endif

#elif defined(__HP_cc) || defined(__HP_aCC)
/* Hewlett-Packard C/aC++. ---------------------------------- */
#define COMPILER_NAME "Hewlett-Packard C/aC++"
#error "not implemented"

#elif defined(__IBMC__) || defined(__IBMCPP__)
/* IBM XL C/C++. -------------------------------------------- */
#define COMPILER_NAME "IBM XL C/C++"
#error "not implemented"

#elif defined(_MSC_VER)
/* Microsoft Visual Studio. --------------------------------- */
#define COMPILER_NAME "Microsoft Visual Studio"
#error "not implemented"

#elif defined(__PGI)
/* Portland Group PGCC/PGCPP. ------------------------------- */
#define COMPILER_NAME "Portland Group PGCC/PGCPP"
#error "not implemented"

#elif defined(__SUNPRO_C) || defined(__SUNPRO_CC)
/* Oracle Solaris Studio. ----------------------------------- */
#define COMPILER_NAME "Oracle Solaris Studio"
#error "not implemented"

#else
#error "Unknown compiler"
#endif

#include <unordered_map>
#include <string>
#include "json.hpp"

using nlohmann::json;

namespace util {

struct meta_info {
    std::string compiler_name = COMPILER_NAME;
    std::string compiler_version = COMPILER_VERSION;
    std::string machine;
    std::string device;
    std::string application;
    std::string routine;
    std::string precision;
    std::string input_size;
    std::unordered_map<std::string, std::string> additional_values;
};

void to_json(json& j, const struct meta_info& m);

void from_json(const json& j, struct meta_info& m);

struct meta_info make_meta_info(const std::string &machine, const std::string &device,
                                const std::string &application, const std::string &routine,
                                util::PRECISION precision, const std::string &input_size);

}

#endif //META_HPP
