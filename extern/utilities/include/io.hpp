//
// Created by Richard Schulze on 03.02.18.
//

#ifndef IO_HPP
#define IO_HPP

#include "json.hpp"
#include "data.hpp"

using nlohmann::json;

namespace util {

bool directory_exists(const std::string &directory);

void mkdirs(const std::string &directory);

std::string &ltrim(std::string &s);

std::string &rtrim(std::string &s);

std::string &trim(std::string &s);

std::string to_simple_string(const std::string &str);

void write_to_file(const std::string &filename, const json &j);

json read_from_file(const std::string &filename);

std::string prepare_directory(const std::string &device, const std::string &application,
                              const std::string &routine, PRECISION precision, const std::string &input_size,
                              bool error_when_exists = true);

std::string hostname();

std::string cpu_name();

std::string exec(const char* cmd);

}

#endif //IO_HPP
