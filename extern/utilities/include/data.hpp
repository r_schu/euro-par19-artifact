//
// Created by Richard Schulze on 03.02.18.
//

#ifndef DATA_HPP
#define DATA_HPP

#include <cstdlib>
#include <string>

namespace util {

enum PRECISION {
    SINGLE, DOUBLE, SIZE_T, INT
};

std::string to_string(PRECISION precision);

size_t size_of(PRECISION precision);

template<typename T>
void fill(T *data, size_t elems, int min = 1, int max = 10) {
    for (int i = 0; i < elems; ++i) {
        data[i] = (i % (max - min + 1)) + min;
    }
}

template<typename T=char>
void fill(T *data, size_t elems, PRECISION precision, int min = 1, int max = 10) {
    for (int i = 0; i < elems; ++i) {
        switch (precision) {
            case SINGLE:
                ((float *)data)[i] = (i % (max - min + 1)) + min;
                break;
            case DOUBLE:
                ((double *)data)[i] = (i % (max - min + 1)) + min;
                break;
            case SIZE_T:
                ((size_t *)data)[i] = (i % (max - min + 1)) + min;
                break;
            case INT:
                ((int *)data)[i] = (i % (max - min + 1)) + min;
                break;
        }
    }
}

}

#endif //DATA_HPP
