//
// Created by Richard Schulze on 11.02.18.
//

#ifndef TUNING_HPP
#define TUNING_HPP

#include <chrono>
#include "json.hpp"
#include "data.hpp"
#include <atf.h>

using nlohmann::json;

namespace util {

struct tuning_update_key {
    std::string machine;
    std::string device;
    std::string application;
    std::string routine;
    std::string precision;
    std::string input_size;
};

void to_json(json& j, const struct tuning_update_key& t);

void from_json(const json& j, struct tuning_update_key& t);

struct tuning_update {
    struct tuning_update_key key;
    std::string status;
    float best_ms;
    size_t tested_configs;
    size_t valid_configs;
    std::string tuner;
    std::string started;
    std::string wrapper;
    std::string warm_up_timeout;
    std::string evaluation_timeout;
};

void to_json(json& j, const struct tuning_update& t);

void from_json(const json& j, struct tuning_update& t);

template<typename tuner_t>
struct tuning_update make_tuning_update(const std::string &machine, const std::string &device,
                                        const std::string &application, const std::string &routine,
                                        util::PRECISION precision, const std::string &input_size,
                                        const std::string &status, unsigned long long best_ns, tuner_t *tuner,
                                        const atf::cf::process_wrapper_info &wrapper_info,
                                        const atf::cf::timeout &warm_up_timeout,
                                        const atf::cf::timeout &evaluation_timeout) {
    struct tuning_update update{};
    update.key.machine = machine;
    update.key.device = device;
    update.key.application = application;
    update.key.routine = routine;
    update.key.precision = to_string(precision);
    update.key.input_size = input_size;
    update.status = status;
    update.best_ms = best_ns / 1000000.0f;
    if (tuner != nullptr) {
        update.tested_configs = tuner->number_of_evaluated_configs();
        update.valid_configs = tuner->number_of_valid_evaluated_configs();
        update.tuner = tuner->display_string_with_abort_condition();
        auto tp = std::chrono::system_clock::to_time_t(tuner->tuning_start_time());
        std::stringstream ss;
        ss << std::put_time(std::localtime(&tp), "%F %T");
        update.started = ss.str();
    }
    switch (wrapper_info.type) {
        case atf::cf::NONE:
            update.wrapper = "none";
            break;
        case atf::cf::LOCAL:
            update.wrapper = "local";
            break;
        case atf::cf::REMOTE:
            update.wrapper = "remote";
            break;
    }
    switch (warm_up_timeout.type) {
        case atf::cf::ABSOLUTE:
            if (warm_up_timeout.value.absolute != 0) {
                update.warm_up_timeout = std::to_string(warm_up_timeout.value.absolute) + "ns (absolute)";
            }
            break;
        case atf::cf::FACTOR:
            if (warm_up_timeout.value.factor != 0.0f) {
                update.warm_up_timeout = std::to_string(warm_up_timeout.value.factor) + " (factor)";
            }
            break;
    }
    if (update.warm_up_timeout.empty()) update.warm_up_timeout = "none";
    switch (evaluation_timeout.type) {
        case atf::cf::ABSOLUTE:
            if (evaluation_timeout.value.absolute != 0) {
                update.evaluation_timeout = std::to_string(evaluation_timeout.value.absolute) + "ns (absolute)";
            }
            break;
        case atf::cf::FACTOR:
            if (evaluation_timeout.value.factor != 0.0f) {
                update.evaluation_timeout = std::to_string(evaluation_timeout.value.factor) + " (factor)";
            }
            break;
    }
    if (update.evaluation_timeout.empty()) update.evaluation_timeout = "none";
    return update;
}

struct tuning_meta_info {
    std::string tuner;
    std::string wrapper;
    std::string warm_up_timeout;
    std::string evaluation_timeout;
    size_t tested_configurations;
    size_t valid_configurations;
    std::string start_timestamp;
};

void to_json(json& j, const struct tuning_meta_info& t);

void from_json(const json& j, struct tuning_meta_info& t);

template<typename tuner_t>
struct tuning_meta_info make_tuning_meta_info(tuner_t *tuner,
                                              const atf::cf::process_wrapper_info &wrapper_info,
                                              const atf::cf::timeout &warm_up_timeout,
                                              const atf::cf::timeout &evaluation_timeout) {
    struct tuning_meta_info meta_info{};
    meta_info.tested_configurations = tuner->number_of_evaluated_configs();
    meta_info.valid_configurations = tuner->number_of_valid_evaluated_configs();
    meta_info.tuner = tuner->display_string_with_abort_condition();
    auto tp = std::chrono::system_clock::to_time_t(tuner->tuning_start_time());
    std::stringstream ss;
    ss << std::put_time(std::localtime(&tp), "%F %T");
    meta_info.start_timestamp = ss.str();
    switch (wrapper_info.type) {
        case atf::cf::NONE:
            meta_info.wrapper = "none";
            break;
        case atf::cf::LOCAL:
            meta_info.wrapper = "local";
            break;
        case atf::cf::REMOTE:
            meta_info.wrapper = "remote";
            break;
    }
    switch (warm_up_timeout.type) {
        case atf::cf::ABSOLUTE:
            if (warm_up_timeout.value.absolute != 0) {
                meta_info.warm_up_timeout = std::to_string(warm_up_timeout.value.absolute) + "ns (absolute)";
            }
            break;
        case atf::cf::FACTOR:
            if (warm_up_timeout.value.factor != 0.0f) {
                meta_info.warm_up_timeout = std::to_string(warm_up_timeout.value.factor) + " (factor)";
            }
            break;
    }
    if (meta_info.warm_up_timeout.empty()) meta_info.warm_up_timeout = "none";
    switch (evaluation_timeout.type) {
        case atf::cf::ABSOLUTE:
            if (evaluation_timeout.value.absolute != 0) {
                meta_info.evaluation_timeout = std::to_string(evaluation_timeout.value.absolute) + "ns (absolute)";
            }
            break;
        case atf::cf::FACTOR:
            if (evaluation_timeout.value.factor != 0.0f) {
                meta_info.evaluation_timeout = std::to_string(evaluation_timeout.value.factor) + " (factor)";
            }
            break;
    }
    if (meta_info.evaluation_timeout.empty()) meta_info.evaluation_timeout = "none";
    return meta_info;
}

}

#endif //TUNING_HPP
