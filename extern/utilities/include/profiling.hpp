//
// Created by Richard Schulze on 03.02.18.
//

#ifndef PROFILING_HPP
#define PROFILING_HPP

#include <cstdlib>
#include <vector>
#include <unordered_map>

#include "json.hpp"

using nlohmann::json;

namespace util {

struct profiling_info {
    unsigned short int warm_ups;
    unsigned short int evaluations;
    unsigned long long avg;
    unsigned long long median;
    unsigned long long min;
    unsigned long long max;
    unsigned long long std_dev;
    std::unordered_map<std::string, std::string> parameter_values;
    std::vector<unsigned long long> runtimes;
};

void to_json(json& j, const struct profiling_info& p);

void from_json(const json& j, struct profiling_info& p);

struct profiling_info make_profiling_info(unsigned short int warm_ups, unsigned short int evaluations,
                                          const std::vector<unsigned long long> &runtimes);

}

#endif //PROFILING_HPP
