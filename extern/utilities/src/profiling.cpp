//
// Created by Richard Schulze on 03.02.18.
//

#include <numeric>
#include <algorithm>
#include "profiling.hpp"

namespace util {

void to_json(json& j, const struct profiling_info& p) {
    j = json{{"warm_ups", p.warm_ups},
             {"evaluations", p.evaluations},
             {"avg", p.avg},
             {"median", p.median},
             {"min", p.min},
             {"max", p.max},
             {"std_dev", p.std_dev},
             {"parameter_values", p.parameter_values},
             {"runtimes", p.runtimes}
    };
}

void from_json(const json& j, struct profiling_info& p) {
    p.warm_ups = j.at("warm_ups").get<unsigned short int>();
    p.evaluations = j.at("evaluations").get<unsigned short int>();
    p.avg = j.at("avg").get<unsigned long long>();
    p.median = j.at("median").get<unsigned long long>();
    p.min = j.at("min").get<unsigned long long>();
    p.max = j.at("max").get<unsigned long long>();
    p.std_dev = j.at("std_dev").get<unsigned long long>();
    p.parameter_values = j.at("parameter_values").get<std::unordered_map<std::string, std::string>>();
    p.runtimes = j.at("runtimes").get<std::vector<unsigned long long>>();
}

struct profiling_info make_profiling_info(unsigned short int warm_ups, unsigned short int evaluations,
                                          const std::vector<unsigned long long> &runtimes) {
    if (runtimes.empty()) {
        return {};
    }
    auto runtimes_copy = runtimes;
    std::sort(runtimes_copy.begin(), runtimes_copy.end());
    unsigned long long median;
    if (runtimes_copy.size() % 2 == 0) {
        median = (runtimes_copy[runtimes_copy.size() / 2 - 1] + runtimes_copy[runtimes_copy.size() / 2]) / 2;
    } else {
        median = runtimes_copy[runtimes_copy.size() / 2];
    }
    struct profiling_info info = {
            .warm_ups = warm_ups,
            .evaluations = evaluations,
            .avg = std::accumulate(runtimes.begin(), runtimes.end(), static_cast<unsigned long long>(0)) / runtimes.size(),
            .median = median,
            .min = *std::min_element(runtimes.begin(), runtimes.end()),
            .max = *std::max_element(runtimes.begin(), runtimes.end()),
            .std_dev = 0,
            .parameter_values = {},
            .runtimes = runtimes
    };
    info.std_dev = static_cast<unsigned long long>(std::sqrt(std::accumulate(
            runtimes.begin(), runtimes.end(), static_cast<unsigned long long>(0),
            [&info] (auto a, auto b) {
                return a + (info.avg - b) * (info.avg - b);
            }) / runtimes.size()));
    return info;
}

}