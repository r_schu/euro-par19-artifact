//
// Created by Richard Schulze on 03.02.18.
//
#include <data.hpp>
#include "meta.hpp"

namespace util {

void to_json(json &j, const struct meta_info &m) {
    j = json{
            {"compiler_name", m.compiler_name},
            {"compiler_version", m.compiler_version},
            {"Machine", m.machine},
            {"Device", m.device},
            {"Application", m.application},
            {"Routine", m.routine},
            {"Precision", m.precision},
            {"Input Size", m.input_size},
            {"additional_values", m.additional_values}
    };
}

void from_json(const json &j, struct meta_info &m) {
    m.compiler_name = j.at("compiler_name").get<std::string>();
    m.compiler_version = j.at("compiler_version").get<std::string>();
    m.machine = j.at("Machine").get<std::string>();
    m.device = j.at("Device").get<std::string>();
    m.application = j.at("Application").get<std::string>();
    m.routine = j.at("Routine").get<std::string>();
    m.precision = j.at("Precision").get<std::string>();
    m.input_size = j.at("Input Size").get<std::string>();
    m.additional_values = j.at("additional_values").get<std::unordered_map<std::string, std::string>>();
}

struct meta_info make_meta_info(const std::string &machine, const std::string &device,
                                const std::string &application, const std::string &routine,
                                util::PRECISION precision, const std::string &input_size) {
    struct meta_info info{};
    info.machine = machine;
    info.device = device;
    info.application = application;
    info.routine = routine;
    info.precision = to_string(precision);
    info.input_size = input_size;
    return info;
}

}