//
// Created by Richard Schulze on 03.02.18.
//

#include "data.hpp"

namespace util {

std::string to_string(PRECISION precision) {
    switch (precision) {
        case SINGLE: return "float";
        case DOUBLE: return "double";
        case SIZE_T: return "size_t";
        case INT: return "int";
    }
}

size_t size_of(PRECISION precision) {
    switch (precision) {
        case SINGLE: return sizeof(float);
        case DOUBLE: return sizeof(double);
        case SIZE_T: return sizeof(size_t);
        case INT: return sizeof(int);
    }
}

}