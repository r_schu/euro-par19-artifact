//
// Created by Richard Schulze on 11.02.18.
//

#include <unordered_map>
#include "tuning.hpp"

namespace util {

void to_json(json& j, const struct tuning_update_key& t) {
    j = json{{"Machine", t.machine},
             {"Device", t.device},
             {"Application", t.application},
             {"Routine", t.routine},
             {"Precision", t.precision},
             {"Input Size", t.input_size},
    };
}

void from_json(const json& j, struct tuning_update_key& t) {
    t.machine = j.at("Machine").get<std::string>();
    t.device = j.at("Device").get<std::string>();
    t.application = j.at("Application").get<std::string>();
    t.routine = j.at("Routine").get<std::string>();
    t.precision = j.at("Precision").get<std::string>();
    t.input_size = j.at("Input Size").get<std::string>();
}

void to_json(json& j, const struct tuning_update& t) {
    j = json{{"key", t.key},
             {"Status", t.status},
             {"Best", t.best_ms},
             {"Tested Configs", t.tested_configs},
             {"Valid Configs", t.valid_configs},
             {"Tuner", t.tuner},
             {"Started", t.started},
             {"Wrapper", t.wrapper},
             {"Warm Up Timeout", t.warm_up_timeout},
             {"Evaluation Timeout", t.evaluation_timeout},
    };
}

void from_json(const json& j, struct tuning_update& t) {
    t.key = j.at("key").get<struct tuning_update_key>();
    t.status = j.at("Status").get<std::string>();
    t.best_ms = j.at("Best").get<float>();
    t.tested_configs = j.at("Tested Configs").get<size_t>();
    t.valid_configs = j.at("Valid Configs").get<size_t>();
    t.tuner = j.at("Tuner").get<std::string>();
    t.started = j.at("Started").get<std::string>();
    t.wrapper = j.at("Wrapper").get<std::string>();
    t.warm_up_timeout = j.at("Warm Up Timeout").get<std::string>();
    t.evaluation_timeout = j.at("Evaluation Timeout").get<std::string>();
}

void to_json(json& j, const struct tuning_meta_info& t) {
    j = json{{"tuner", t.tuner},
             {"wrapper", t.wrapper},
             {"warm_up_timeout", t.warm_up_timeout},
             {"evaluation_timeout", t.evaluation_timeout},
             {"tested_configurations", t.tested_configurations},
             {"valid_configurations", t.valid_configurations},
             {"start_timestamp", t.start_timestamp},
    };
}

void from_json(const json& j, struct tuning_meta_info& t) {
    t.tuner = j.at("tuner").get<std::string>();
    t.wrapper = j.at("wrapper").get<std::string>();
    t.warm_up_timeout = j.at("warm_up_timeout").get<std::string>();
    t.evaluation_timeout = j.at("evaluation_timeout").get<std::string>();
    t.tested_configurations = j.at("tested_configurations").get<size_t>();
    t.valid_configurations = j.at("valid_configurations").get<size_t>();
    t.start_timestamp = j.at("start_timestamp").get<std::string>();
}

}