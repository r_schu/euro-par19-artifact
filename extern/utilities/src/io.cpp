//
// Created by Richard Schulze on 03.02.18.
//
#include "io.hpp"

#include <fstream>
#include <sys/stat.h>
#include <iostream>
#include <unistd.h>
#include <climits>

#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

#include <regex>

namespace util {

bool directory_exists(const std::string &directory) {
    struct stat info{};
    return stat(directory.c_str(), &info) == 0 && (info.st_mode & S_IFDIR) != 0;
}

void mkdirs(const std::string &directory) {
    int error = 0;
    size_t start_pos = 0, end_pos;
    while ((end_pos = directory.find('/', start_pos)) != std::string::npos) {
        if (start_pos != end_pos) {
            error = mkdir(directory.substr(0, end_pos).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            if (error != 0 && errno != EEXIST) {
                std::cerr << "error while creating directory: " << strerror(errno) << std::endl;
                exit(EXIT_FAILURE);
            }
        }
        start_pos = end_pos + 1;
    }
    error = mkdir(directory.substr(0, directory.length()).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (error != 0 && errno != EEXIST) {
        std::cerr << "error while creating directory: " << strerror(errno) << std::endl;
        exit(EXIT_FAILURE);
    }
}

// trim from start
std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

std::string to_simple_string(const std::string &str) {
    std::string simple_str = str;
    for (char &i : simple_str) {
        if ((i < 48 || i > 57)
            && (i < 65 || i > 90)
            && (i < 97 || i > 122)) {
            i = '_';
        }
    }
    return simple_str;
}

void write_to_file(const std::string &filename, const json &j) {
    std::ofstream f_out(filename, std::ios::out | std::ios::trunc);
    f_out << std::setw(4) << j;
    f_out.close();
}

json read_from_file(const std::string &filename) {
    std::ifstream f_in(filename, std::ios::in);
    return json::parse(f_in);
}

std::string prepare_directory(const std::string &device, const std::string &application,
                              const std::string &routine, PRECISION precision, const std::string &input_size,
                              bool error_when_exists) {
    std::string path = to_simple_string(device)
                       + "/" + to_simple_string(application)
                       + "/" + to_simple_string(routine)
                       + "/" + to_simple_string(to_string(precision))
                       + "/" + to_simple_string(input_size);

    // create directory
    if (directory_exists(path)) {
        std::cerr << "output directory " << path << " already exists" << std::endl;
        if (error_when_exists)
            exit(EXIT_FAILURE);
    }
    mkdirs(path);

    return path;
}

std::string hostname() {
    // get host name
    char hostname[HOST_NAME_MAX];
    if (gethostname(hostname, HOST_NAME_MAX) != 0) {
        std::cerr << "cannot get hostname" << std::endl;
        exit(EXIT_FAILURE);
    }
    return std::string(hostname);
}

std::string cpu_name() {
    std::ifstream cpu_info_file;
    cpu_info_file.open("/proc/cpuinfo", std::ifstream::in);
    std::string cpu_info((std::istreambuf_iterator<char>(cpu_info_file)),
                         std::istreambuf_iterator<char>());
    cpu_info_file.close();
    std::regex rgx(R"(model name.*: (.+))");
    std::sregex_iterator rgx_begin(cpu_info.begin(), cpu_info.end(), rgx);
    std::sregex_iterator rgx_end;
    if (rgx_begin != rgx_end) {
        return rgx_begin->str(1);
    } else {
        std::cerr << "cpu name not found" << std::endl;
        exit(EXIT_FAILURE);
    }
}

std::string exec(const char* cmd) {
    std::array<char, 128> buffer{};
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
            result += buffer.data();
    }
    return result;
}

}